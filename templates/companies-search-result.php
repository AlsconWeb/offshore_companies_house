<div id="search-app" class="col-xs-12">
    
    <form class="form-horizontal" method="get" action="/blog/company_house">
        <input type="search" name="search_company" class="form-control" placeholder="Введите адрес компании"
               value="<?= isset($_GET['search_company']) ? $_GET['search_company'] : '' ?>">
        <input type="submit" value="Поиск">
    </form>
</div> 

<?php
	//echo nl2br(print_r($companies, true));
	if (!empty($companies)) {
?>
<?php
		foreach ($companies as $type => $companies_list) { if($type == 'exact') continue;
			switch ($type) {
				case 'exact':
					$title = CP_Helper::getTranslation('Exact match:');
					break;
				case 'match':
					$title = CP_Helper::getTranslation('Non-exact matches:');
					break;
			};
?>
<h2><?php //echo $title; ?></h2>
<?php
			if (!empty($companies_list)) {
?>
<div class="search-results-list">
	    <h2>Результаты поиска</h2>
        <?php //$pages = floor($data->total_results / 20) ?>
        <h3>Найдено: <?= $companies['total'] ?> </h3>
<ul><!-- id="ch-companies-result"-->
<?php
				if ($type == 'exact') $companies_list = array($companies_list);
				foreach ($companies_list as $company) {
?>
	<li><a href="<?php echo CP_Helper::getCompanyDetailsUrl($company['number']); ?>" class="search-result-item"><?php echo $company['name']; ?></a>
			<p class="title"><?php echo $company['status']; ?></p>
           <p class="title"><?php echo $company['address']; ?></p>
    </li>
<?php
				};
?>
</ul>
</div>
<?php
			} else {
?>
	<p class="no-results"><?php echo CP_Helper::getTranslation('No matched companies'); ?></p>
<?php
			};
		};
?>
<?php
	} else {
?>
<?php echo sprintf(CP_Helper::getTranslation('No companies found for "%s"'), $name); ?>
<?php
	}
