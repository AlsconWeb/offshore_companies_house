<!--noindex-->
<form name="ch_form" method="post" action="<?php echo get_permalink(CP_Helper::getPageId('company_panama')); ?>">
<label for="ch-company-name"><?php echo CP_Helper::getTranslation('Company name'); ?></label>
<input type="text" name="search_company" placeholder="<?php echo CP_Helper::getTranslation('Please type company name there'); ?>" style="width: 280px;">
<button type='submit'><?php echo CP_Helper::getTranslation('Search'); ?></button>
</form>
<!--/noindex-->