<?
$yes_no = array(
	"0"	=> "No",
	"1"	=> "Yes"
);
$offtypes = array(
	'DIS'	=> 'Disqualified Directors only',
	'LLP'	=> 'Limited Liability Partnerships',
	'CUR'	=> 'Not above',
	'EUR'	=> 'SE and ES appointments only'
);
?>
<div class="wrap">
    <h2>WP Companies House</h2>
    <form method="post" action="options.php"> 
        <?php @settings_fields('wp_companies_house-group'); ?>
        <?php @do_settings_fields('wp_companies_house-group'); ?>

		<h3>XML Gateway Login Details</h3>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_username">Username</label></th>
                <td><input type="text" name="ch_xml_username" id="ch_xml_username" value="<?php echo get_option('ch_xml_username'); ?>" placeholder="XMLGatewayTestUserID" /></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="ch_xml_password">Password</label></th>
                <td><input type="text" name="ch_xml_password" id="ch_xml_password" value="<?php echo get_option('ch_xml_password'); ?>" placeholder="XMLGatewayTestPassword" /></td>
            </tr>
        </table>
		<h3>XML Gateway Request Info</h3>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_email">Email</label></th>
                <td><input type="text" name="ch_xml_email" id="ch_xml_email" value="<?php echo get_option('ch_xml_email'); ?>" placeholder="email@email.com" /><p class="description">May be used by the Gateway for auditing and as a secondary point of contact in the event of failure</p></td>
            </tr>
		</table>
		<h3>Company Details</h3>
		<p class="description">Queries gateway for details about a specific company, given its Companies House company number.</p>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_morttotals">Give Mort Totals</label></th>
                <td><select name="ch_xml_morttotals" id="ch_xml_morttotals">
<?php 
foreach ($yes_no as $value => $title) {
	$selected = (get_option("ch_xml_morttotals") == $value) ? ' selected="selected"' : ''; 
	echo "<option value='$value'$selected>" . CH_Helper::getTranslation($title) . "</option>";
};
?>
					</select>
					<p class="description">Requests the return of the Mortgages element. Totals are only returned if applicable.</p></td>
            </tr>
		</table>
		<h3>Company Appointments</h3>
		<p class="description">Queries gateway for information about current/resigned company appointments, given its Companies House company number.</p>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_appresigned">Include Resigned</label></th>
                <td><select name="ch_xml_appresigned" id="ch_xml_appresigned">
<?php 
foreach ($yes_no as $value => $title) {
	$selected = (get_option("ch_xml_appresigned") == $value) ? ' selected="selected"' : ''; 
	echo "<option value='$value'$selected>" . CH_Helper::getTranslation($title) . "</option>";
};
?>
					</select>
					<p class="description">Indicates the status of appointments to be returned. Yes = All appointment types returned. No = Only current appointments returned.</p></td>
            </tr>
		</table>
		<h3>Officer Search</h3>
		<p class="description">Queries directory of past and present directors and secretaries.</p>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_offtype">Officer Type</label></th>
                <td><select name="ch_xml_offtype" id="ch_xml_offtype">
<?php 
foreach ($offtypes as $value => $title) {
	$selected = (get_option("ch_xml_offtype") == $value) ? ' selected="selected"' : ''; 
	echo "<option value='$value'$selected>" . CH_Helper::getTranslation($title) . "</option>";
};
?>
					</select>
					<p class="description">Not above == Current</p></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="ch_xml_offresigned">Include Resigned</label></th>
                <td><select name="ch_xml_offresigned" id="ch_xml_offresigned">
<?php 
foreach ($yes_no as $value => $title) {
	$selected = (get_option("ch_xml_offresigned") == $value) ? ' selected="selected"' : ''; 
	echo "<option value='$value'$selected>" . CH_Helper::getTranslation($title) . "</option>";
};
?>
					</select>
					<p class="description">Indicate whether resigned officer should be returned. Yes = All officer types returned. No = Only current officer returned.</p></td>
            </tr>
		</table>
		<h3>WooCommerce Products</h3>
		<p class="description">WooCommerce products to use for Documents and Officer details purchases</p>
        <table class="form-table">  
            <tr valign="top">
                <th scope="row"><label for="ch_xml_doc_product">Document Product ID</label></th>
                <td><input type="text" name="ch_xml_doc_product" id="ch_xml_doc_product" value="<?php echo get_option('ch_xml_doc_product'); ?>" placeholder="<ID> there" /><p class="description">Document product. Price will be incrased based on document count</p></td>
            </tr>
            <tr valign="top">
                <th scope="row"><label for="ch_xml_off_product">Officer Details Product ID</label></th>
                <td><input type="text" name="ch_xml_off_product" id="ch_xml_off_product" value="<?php echo get_option('ch_xml_off_product'); ?>" placeholder="<ID> there" /><p class="description">Document product. Price will be incrased based on document count</p></td>
            </tr>
		</table>

        <?php @submit_button(); ?>
    </form>
</div>
