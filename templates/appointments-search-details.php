<?php if (!empty($details)) { //print_r($details); ?>
<table id="ch-company-details">
<?php 	foreach ($details as $name => $value) {  ?>
<?php 		if (!is_array($value)) { ?>
<tr>
	<td><?php echo CP_Helper::getTranslation($name); ?></td>
	<td><?php echo $value; ?></td>
</tr>
<?php 		} else { ?>
<tr>
	<th colspan="2"><?php echo CP_Helper::getTranslation($name); ?>
</tr>
<?php 			foreach ($value as $name2 => $value2) {  ?>
<?php 				if (in_array(strtolower($name2), array('person id', 'surname', 'forename'))) continue; ?>
<tr>
	<td><?php echo CP_Helper::getTranslation($name2); ?></td>
	<td><?php echo $value2; ?></td>
</tr>
<?php 			}; /*?>
    <tr>
        <td style="white-space: nowrap;"><?php echo CP_Helper::getTranslation('View officer details'); ?></td>
        <td colspan="4" class='woocommerce'><a class="button add_documents product_type_simple add_to_cart_button"
            data-product_id="<?php echo CP_Helper::getOption('ch_xml_off_product'); ?>"
            data-officer_number="<?php echo $value['Person Id']; ?>"
            data-officer_name="<?php echo ((array_key_exists('Title', $value)) ? $value['Title'] : ''). ' ' . $value['Forename'] . ' ' . $value['Surname']; ?>"
            ><?php echo __('Добавить в корзину', 'wp-companies-house'); ?></a>
        </td>
    </tr>
<?php		*/}; ?>
<?php 	}; ?>
</table>
<?php } else { ?>
<?php echo CP_Helper::getTranslation('No appointment details found'); ?>
<?php } ?>
