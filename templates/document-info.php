<?php
	//echo nl2br(print_r($companies, true));
	if (!empty($document)) {
?>
<table id="document-info" class="woocommerce">
<?php
		$i = 0;
		foreach ($document as $field_name => $field_value) {
			if ($field_name == 'key') continue;
			if ($field_name == 'doc_key') continue;
			if ($field_name == 'date') {
				$field_value = date("Y-m-d", $field_value);
			};
?>
<tr>
	<th><?php echo __(CH_Helper::parseFieldName($field_name)); ?></th>
	<td><?php echo $field_value; ?></td>
</tr>
<?php
		};
		if (array_key_exists('key', $document) && !empty($document['key'])) {
			$cart_doc_name = $document['type'] . ': ' . $document['description'];
?>
<tr>
	<th><?php echo __('Price is 1 &pound;'); ?></th>
	<td><a class="button add_to_cart_button product_type_simple" 
		name="add_doc_to_cart" 
		data-document_key="<?php echo base64_encode($document['doc_key']); ?>" 
		data-document_name="<?php echo $cart_doc_name; ?>" 
		data-product_id="<?php echo CH_Helper::getOption('ch_xml_doc_product'); ?>"
		data-company_number="<?php echo $companyNumber; ?>"
		data-company_name="<?php echo $companyName; ?>"
		type="button" 
		id="add_doc"><?php echo __('Добавить в корзину'); ?></button>
	</td>
</tr>
<?php
		};
?>
</table>
<?php
	} else {
?>
<?php echo sprintf(CH_Helper::getTranslation('No documents found for "%s"'), $name); ?>
<?php
	}
