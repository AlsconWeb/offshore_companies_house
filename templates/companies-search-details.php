<?php if (!empty($details)) { ?>
<table id="ch-company-details">
<?php 	foreach ($details as $name => $value) {  ?>
<tr>
	<td><?php echo CP_Helper::getTranslation($name); ?></td>
	<td><?php echo $value; ?></td>
</tr>
<?php 	}; ?>
</table>
<?php } else { ?>
<?php echo CP_Helper::getTranslation('No company found'); ?>
<?php } ?>
