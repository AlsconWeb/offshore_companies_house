<?php if (!empty($details)) { ?>
<table id="ch-company-details">
<?php 	foreach ($details as $name => $value) {  ?>
<?php 		if (!is_array($value)) { ?>
<tr>
	<td><?php echo CP_Helper::getTranslation($name); ?></td>
	<td><?php echo $value; ?></td>
</tr>
<?php 		} else { ?>
<tr>
	<th colspan="2"><?php echo CP_Helper::getTranslation($name); ?>
</tr>
<?php 			foreach ($value as $name2 => $value2) {  ?>
<tr>
	<td><?php echo CP_Helper::getTranslation($name2); ?></td>
	<td><?php echo $value2; ?></td>
</tr>
<?php 			}; ?>
<?php		}; ?>
<?php 	}; ?>
</table>
<?php } else { ?>
<?php CP_Helper::getTranslation('No officer found'); ?>
<?php } ?>
