<?php
	//echo print_r($officers, true);
//die();
	$ignored_fields = array('forename', 'surname', 'title', 'id');
	if (!empty($officers)) {
?>
<?php
		foreach ($officers as $type => $officer_list) { if($type == 'exact') continue;
			if (!in_array($type, array('exact', 'match'))) continue;
			switch ($type) {
				case 'exact':
					$title = CP_Helper::getTranslation('Exact match:');
					break;
				case 'match':
					$title = CP_Helper::getTranslation('Non-exact matches:');
					break;
			};
?>
<h2><?php //echo $title; ?></h2>
<?php
			if (!empty($officer_list)) {
?>
<?php 			if ($type == 'exact') $officer_list = array($officer_list); ?>
<table class="ch-officer-result"  style="border-collapse: collapse; border-bottom: none;">
<?php				foreach ($officer_list as $okey => $officer) {?>
<tr>
	<th colspan='2'><?php 
	
					$link = get_permalink(CP_Helper::getPageId('officer_panama'));
					$link = add_query_arg('officer_id', base64_encode($officer['id']), $link);
					$value = "<a href='$link' class='ap-details-link'>".$officer['title'] . ' ' . $officer['forename'] . ' ' . $officer['surname']."</a>" ;
	
					echo $value; ?></th>
</tr>
<?php				foreach ($officer as $key => $value) { if (in_array($key, $ignored_fields) || empty($value)) continue;?>
<?php					if (is_array($value)) { ?>
<tr>
	<td colspan=2>&nbsp;</td>
</tr>
<tr>
	 <th><?php echo CP_Helper::getTranslation(CP_Helper::parseFieldName($key)); ?></th>
	 <td><table>
<?php						foreach ($value as $item) { ?>
		<tr>
    		<th colspan='2'><?php echo $officer['title'] . ' ' . $officer['forename'] . ' ' . $officer['surname']; ?></th>
		</tr>
<?php							foreach ($item as $key2 => $value2) { if (in_array($key2, $ignored_fields) || empty($value2)) continue;?>
		<tr>
			<td><?php echo CP_Helper::getTranslation(CP_Helper::parseFieldName($key2)); ?></td>
			<td><?php echo CP_Helper::parseFieldValue($key2, $value2); ?></td>
		</tr>

<?php							}; ?>
<?php						}; ?>
	 </table></td>
</tr>
<?php					} else { ?>
<tr>
	<td><?php echo CP_Helper::getTranslation(CP_Helper::parseFieldName($key)); ?></td>
	<td><?php echo CP_Helper::parseFieldValue($key, $value); ?></td>
</tr>
<?php					}; ?>
<?php				}; /*?>
    <tr>
		<td style="white-space: nowrap;"><?php echo CP_Helper::getTranslation('View officer details'); ?></td>
        <td colspan="4" class='woocommerce'><a class="button add_documents product_type_simple add_to_cart_button"
            data-product_id="<?php echo CP_Helper::getOption('ch_xml_off_product'); ?>"
            data-officer_number="<?php echo $officer['id']; ?>"
            data-officer_name="<?php echo $officer['title'] . ' ' . $officer['forename'] . ' ' . $officer['surname']; ?>"
            ><?php echo __('Добавить в корзину', 'wp-companies-house'); ?></a>
		</td>
    </tr><?php 			*/ ?>
<tr>
	<th colspan='2'>&nbsp;</th>
</tr>
<?php 			}; ?>
</table>
<?php
			} else {
?>
	<p class="no-results"><?php echo CP_Helper::getTranslation('No matched officers'); ?></p>
<?php
			};
		};
?>
<?php
	} else {
?>
<?php echo sprintf(CP_Helper::getTranslation('No officer found for "%s"'), $name); ?>
<?php
	}
