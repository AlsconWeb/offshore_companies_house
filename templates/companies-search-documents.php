<?php
	//echo nl2br(print_r($companies, true));
	if (!empty($documents)) {
		$cart_active = true;
?>
<table id="company-filing-history">
<tr>
	<?php if ($cart_active) : ?>
	<th><input type="checkbox" onClick="change_selection(this);"></th>
	<?php endif; ?>
	<th><?php echo __('Date');?></th>
	<th><?php echo __('Type');?></th>
	<th><?php echo __('Description');?></th>
</th>
<?php
		$i = 0;
		$doc_count = 0;
		foreach ($documents as $doc) {
?>
<tr>
	<?php if ($cart_active) : ?>
	<td>
		<?php if (isset($doc['key'])) : ?>
		<?php $doc_count++; ?>
		<input type="checkbox" name="documents[]" value="<?php echo base64_encode($doc['key']); ?>"
			data-document_name="<?php echo $doc['type'] . ':' . implode("<br />", $doc['description']); ?>"
			data-document_key="<?php echo base64_encode($doc['key']); ?>"
		>
		<?php endif; ?>
	</td>
	<?php endif; ?>
	<td><?php echo date("Y-m-d", $doc['date']); ?></td>
	<td><?php echo $doc['type']; ?></td>
	<td>
	<?php if (isset($doc['key']) && FALSE) { ?>
		<a href="<?php echo add_query_arg('company_doc', base64_encode($doc['key'])); ?>">
	<?php }; ?>
			<?php echo implode("<br />", $doc['description']); ?>
	<?php if (isset($doc['key']) && FALSE) { ?>
		</a>
	<?php }; ?>
	</td>
	<?php if ($cart_active && false) : ?>
	<td>
		<?php if (isset($doc['key'])) : ?>
		<button type="button" data-value="<?php echo base64_encode($doc['key']); ?>"><?php echo __('Добавить в корзину', 'wp-companies-house'); ?></button>
		<?php endif; ?>
	</td>
	<?php endif; ?>

</tr>
<?php
		};
	if ($doc_count) {
?>
	<tr>
		<td colspan="4" class='woocommerce'><a class="button add_documents product_type_simple add_to_cart_button"
			data-product_id="<?php echo CH_Helper::getOption('ch_xml_doc_product'); ?>"
			data-company_number="<?php echo $company_number; ?>"
			data-company_name="<?php echo $company_name; ?>"
target="_blank"
href="http://test.com"
			><?php echo __('Добавить в корзину', 'wp-companies-house'); ?></a>
	</tr>	
<?php
	}
?>
</table>
<?php
	} else {
?>
<?php echo sprintf(CH_Helper::getTranslation('No documents found for "%s"'), $name); ?>
<?php
	}

?>
<script>
jQuery(document).ready(function () {
a = jQuery;
jQuery('.add_documents').prop('target', "_self").prop("href", "");
jQuery('.add_documents').click(function(e) {
	e.stopPropagation();
	if (a("input[name='documents[]']:checked").length == 0) {
		alert('<?php echo CH_Helper::getTranslation('No documents selected'); ?>');
		return false;
	};
	var b = a(this);
	if (b.is(".product_type_simple")) {
		if (!b.attr("data-product_id")) return !0;
		b.removeClass("added"), b.addClass("loading");
		var c = {
			action: "woocommerce_add_to_cart"
		};
		return a.each(b.data(), function(a, b) {
			c[a] = b
		}), a("input[name='documents[]']:checked").each(function () {
			a.each(a(this).data(), function (a, b) {
				if (!c.hasOwnProperty(a)) {
					c[a] = [];
				};
				c[a].push(b);
			})
			a(this).prop('checked', false);
		}), a("body").trigger("adding_to_cart", [b, c]), a.post(wc_add_to_cart_params.ajax_url, c, function(c) {
			if (c) {
				var d = window.location.toString();
				return d = d.replace("add-to-cart", "added-to-cart"), c.error && c.product_url ? void(window.location = c.product_url) : "yes" === wc_add_to_cart_params.cart_redirect_after_add ? void(window.location = wc_add_to_cart_params.cart_url) : (b.removeClass("loading"), fragments = c.fragments, cart_hash = c.cart_hash, fragments && a.each(fragments, function(b) {
					a(b).addClass("updating")
				}), a(".shop_table.cart, .updating, .cart_totals").fadeTo("400", "0.6").block({
					message: null,
					overlayCSS: {
						opacity: .6
					}
				}), b.addClass("added"), wc_add_to_cart_params.is_cart || 0 !== b.parent().find(".added_to_cart").size() || b.after('<a' + ' href' + '="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + "</a>"), fragments && a.each(fragments, function(b, c) {
					a(b).replaceWith(c)
				}), a(".widget_shopping_cart, .updating").stop(!0).css("opacity", "1").unblock(), a(".shop_table.cart").load(d + " .shop_table.cart:eq(0) > *", function() {
					a(".shop_table.cart").stop(!0).css("opacity", "1").unblock(), a("body").trigger("cart_page_refreshed")
				}), a(".cart_totals").load(d + " .cart_totals:eq(0) > *", function() {
					a(".cart_totals").stop(!0).css("opacity", "1").unblock()
				}), a("body").trigger("added_to_cart", [fragments, cart_hash]), void 0)
			}
		}), !1
	}
	return !0
});
});
function change_selection (el) {
	if (el.checked != false) {
		 a("input[name='documents[]']").prop('checked', 'checked');
	} else {
		 a("input[name='documents[]']").prop('checked', false);
	}
}
</script>
