<?php
	class CP_Helper {
		static function getPageId($name) {
			return get_option( self::getPageOptionName($name) );
		}
		static function getPageOptionName($slug) {
			$name = str_replace("-", '_', $slug);
			return 'ch_page_' . $name . '_id';
		}
		static function isPluginPage($post) {
			return self::isCompanyPage($post) || self::isOfficerPage($post);
		}
		static function isCompanyPage($post) {
			$pages = self::getPages();
			return self::isPage($pages[0]/*'company'*/, $post);
		}
		static function isOfficerPage($post) {
			$pages = self::getPages();
			return self::isPage($pages[1]/*'officer'*/, $post);
		}
		static function getPages() {
			$pages_tmp = CP_Constants::getConstant('pages');
			$pages = array();
			foreach ($pages_tmp as $slug => $page) {
				$pages[] = $slug;
			}
			return $pages;
		}
		static function isPage($name, $post) {
			return self::getPageId( $name ) == $post->ID;
		}
		static function getTemplate($file, $args = array()) {
			$filename = CP_PLUGIN_PATH . '/templates/' . $file;
			if (file_exists($filename) && is_readable($filename)) {
				extract($args);
				ob_start();
				include ($filename);
				$content = ob_get_contents();
				ob_end_clean();
				return $content;
			};
			return false;
		}
		static function getCompanyDetailsUrl($company_number) {
			//return add_query_arg('company_number', $company_number, get_permalink(self::getPageId('company_panama')));
			return '/blog/company/'/*get_permalink(self::getPageId('company')) */. $company_number;
		}
		static function getOfficerDetailsUrl($officer_id) {
			return add_query_arg('officer_id', base64_encode($officer_id), get_permalink(self::getPageId('officer_panama')));
		}
		static function parseFieldName($field) {
			switch ($field) {
				case ('posttown') : 
					$field = 'town';
					break;
			};
			return ucwords(str_replace("_", " ", $field));
		}
		static function parseFieldValue($name, $value) {
			switch ($name) {
				case ('dob'):
					if (!is_numeric($value)) {
						list($year, $month, $day) = explode("-", $value);
						$value = mktime(1,1,1,$month,$day,$year);
					}
					return date(get_option('date_format'), $value);
					break;
			};
			return $value;
		}
		static function getOption($option_name = null) {
			static $_defaults = array(
            	'ch_xml_username'		=> 'XMLGatewayTestUserID',
            	'ch_xml_password'		=> 'XMLGatewayTestPassword',
            	'ch_xml_email'			=> '',
            	'ch_xml_morttotals'		=> '0',
            	'ch_xml_appresigned'	=> '0',
            	'ch_xml_offtype'		=> 'CUR',
            	'ch_xml_offresigned'	=> '0',
            	'ch_xml_doc_product'	=> '',
            	'ch_xml_off_product'	=> '',
			);
			if (is_null($option_name)) {
				return $_defaults;
			}
			$option_value = get_option($option_name);
			if ($option_value === FALSE && array_key_exists($option_name, $_defaults)) {
				$option_value = $_defaults[$option_name];
			};
			return $option_value;
		}
		static function getOptions() {
			return self::getOption();
		}
		static function getTranslation($text) {
			//CP_Helper::saveLog($text);
			return __($text, 'companies-house');
		}
		static function saveLog($text) {
			$fp = fopen(dirname(__FILE__) . '/log.php', 'a+');
			fputs($fp, "CP_Helper::getTranslations(\"$text\");\n");
			fclose($fp);
		}
	}
?>
