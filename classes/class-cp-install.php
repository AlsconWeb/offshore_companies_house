<?php
	class CP_Install {
		static function install() {
			$pages = CP_Constants::getConstant('pages');
			/*$options = CP_Helper::getOptions();
			foreach ($options as $key => $value) {
				add_option($key, $value);
			};*/
			foreach ($pages as $slug => $page) {
				self::createPage($slug, $page);
			};
		}
		static function deinstall() {
			$pages = CP_Constants::getConstant('pages');
			$options = CP_Helper::getOptions();
			foreach ($options as $key => $value) {
				delete_option($key);
			};
			foreach ($pages as $slug => $page) {
				self::removePage($slug, true);
			};
			
		}
		static function removePage($slug, $with_option = false) {
			// get name for option
			$option = CP_Helper::getPageOptionName($slug);
			// get value
			$option_value = get_option( $option );
			if ($option_value > 0) {
				wp_delete_post($option_value, true);
			}
			if ($with_option) {
				delete_option($option);
			}
		}
		static function createPage($slug, $page) {
    		global $wpdb;
			
			$option = CP_Helper::getPageOptionName($slug);
			
    		$option_value = get_option( $option );

			if ($option_value === FALSE) {
				// no option exists - create
				add_option($option, 0);
			};

    		if ( $option_value > 0 && get_post( $option_value ) )
        		return -1;
		
        	$page_found = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM " . $wpdb->posts . " WHERE post_type='page' AND post_name = %s LIMIT 1;", $slug ) );


    		if ( $page_found ) {
        		if ( ! $option_value ) {
            		update_option( $option, $page_found );
        		}

        		return $page_found;
    		}

    		$page_data = array(
        		'post_status'       => 'publish',
        		'post_type'         => 'page',
        		'post_author'       => 1,
        		'post_name'         => $slug,
        		'post_title'        => $page['title'],
        		'post_content'      => '',
        		'comment_status'    => 'closed'
    		);
    		$page_id = wp_insert_post( $page_data );
		
    		if ( $option ) {
        		update_option( $option, $page_id );
    		}

			return $page_id;
		}
	}
?>
