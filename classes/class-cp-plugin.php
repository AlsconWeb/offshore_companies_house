<?php

class CP_Plugin {
	private $post_content = null;

	static public function init() {
		static $_instance = null;
		if ( is_null( $_instance ) ) {
			$_instance = new CP_Plugin();
		}
		switch ( true ) {
			case ( array_key_exists( 'search_company', $_POST ) && ! empty( $_POST['search_company'] ) ) :
				wp_safe_redirect( add_query_arg( [ 'search_company' => rawurlencode( trim( $_POST['search_company'] ) ) ] ) );
				exit();
				break;
			case ( array_key_exists( 'search_officer', $_POST ) && ! empty( $_POST['search_officer'] ) ) :
				wp_safe_redirect( add_query_arg( [ 'search_officer' => rawurlencode( trim( $_POST['search_officer'] ) ) ] ) );
				exit();
				break;
		}
		add_action( 'the_post', [ $_instance, 'showCompanyPagePanama' ] );
		add_action( 'the_post', [ $_instance, 'showOfficerPagePanama' ] );
		add_filter( 'the_content', [ $_instance, 'getContent' ] );
		//add_filter('the_title', array($_instance, 'getTitle'));
		add_filter( 'aioseop_title_page', [ $_instance, 'seoTitle' ] );
		add_filter( 'woocommerce_get_item_data', [ $_instance, 'woocommerce_item_data' ], 10, 2 );
		add_filter( 'woocommerce_cart_item_name', [ $_instance, 'woocommerce_item_name' ], 10, 3 );
		// add company_number to cart_item_data
		add_filter( 'woocommerce_add_cart_item_data', [ $_instance, 'woocommerce_add_cart_item_data' ], 10, 3 );
		// set qty to be always 1
		add_filter( 'woocommerce_after_cart_item_quantity_update', [
			$_instance,
			'woocommerce_after_cart_item_quantity_update',
		], 10, 3 );
		// place needed variables to cart object
		add_filter( 'woocommerce_add_cart_item', [ $_instance, 'woocommerce_add_cart_item' ], 10, 2 );
		// place needed variables to cart object
		add_filter( 'woocommerce_cart_item_product', [ $_instance, 'woocommerce_cart_item_product' ], 10, 3 );
		//add_filter('woocommerce_before_cart_table', array($_instance, 'woocommerce_before_cart_table'), 10);
		//add_filter('woocommerce_checkout_update_order_review', array($_instance, 'woocommerce_checkout_update_order_review'), 10, 1);
		add_filter( 'woocommerce_before_calculate_totals', [
			$_instance,
			'woocommerce_before_calculate_totals',
		], 10, 1 );
		// ordered items
		add_filter( 'woocommerce_order_item_name', [ $_instance, 'woocommerce_item_name' ], 10, 2 );
		add_filter( 'woocommerce_add_order_item_meta', [ $_instance, 'woocommerce_add_order_item_meta' ], 10, 3 );
		// remove item from product in cart
		//add_filter('woocommerce_get_remove_url', array($_instance, 'woocommerce_get_remove_url'), 10, 3);
		// process remove item request
		add_action( 'parse_request', [ $_instance, 'parse_request' ], 99 );
		//add_action( 'pre_get_posts', array( $_instance, 'parse_request' ) , 5);
		// download filter
		add_filter( 'woocommerce_order_get_items', [ $_instance, 'woocommerce_order_get_items' ], 10, 2 );
		//add_filter('woocommerce_order_item_meta_end', array($_instance, 'woocommerce_order_item_meta_end'), 10, 3);
		add_filter( 'woocommerce_order_again_cart_item_data', [
			$_instance,
			'woocommerce_order_again_cart_item_data',
		], 10, 3 );
		add_filter( 'woocommerce_get_item_downloads', [ $_instance, 'woocommerce_get_item_downloads' ], 10, 3 );

		return;
	}

	public function woocommerce_order_again_cart_item_data( $cart_item_data, $source_item, $order ) {
		$keys = [ 'company_number', 'company_name', 'ch_documents', 'officer_number', 'officer_name' ];
		foreach ( $keys as $key ) {
			if ( array_key_exists( $key, $source_item ) ) {
				$cart_item_data[ $key ] = $source_item[ $key ];
				if ( $key == 'ch_documents' ) {
					$source_item[ $key ]    = maybe_unserialize( $source_item[ $key ] );
					$_POST['document_key']  = [];
					$_POST['document_name'] = [];
					foreach ( $source_item[ $key ] as $dkey => $dname ) {
						$_POST['document_key'][]  = $dkey;
						$_POST['document_name'][] = $dname;

					}
				}
			}
		}
		$_POST = array_merge( $_POST, $cart_item_data );

		return $cart_item_data;
	}

	public function woocommerce_order_get_items( $items, $order ) {
		foreach ( $items as $item_id => $item ) {
			// bypass items without company_number set
			if ( ! array_key_exists( 'company_number', $item ) || empty( $item['company_number'] ) ) {
				continue;
			};

			$item['ch_documents'] = maybe_unserialize( $item['ch_documents'] );
			// bypass processing for non downloadable orders
			if ( ! $order->is_download_permitted() ) {
				// show only doc names
				$items[ $item_id ]['item_meta']['documents'] = [];
				foreach ( $item['ch_documents'] as $key => $name ) {
					$items[ $item_id ]['item_meta']['documents'][] = $name;
				}

				continue;
			}
			if ( array_key_exists( 'ch_files', $items[ $item_id ] ) ) {
				$items[ $item_id ]['ch_files'] = maybe_unserialize( $items[ $item_id ]['ch_files'] );
			}
			$items[ $item_id ]['ch_files'] = $this->get_product_files( $items[ $item_id ] );
			wc_update_order_item_meta( $item_id, '_ch_files', $items[ $item_id ]['ch_files'] );
			$items[ $item_id ]['item_meta']['downloads'] = [];
			foreach ( $item['ch_documents'] as $key => $name ) {
				if ( array_key_exists( $key, $items[ $item_id ]['ch_files'] ) && ! empty( $items[ $item_id ]['ch_files'][ $key ] ) ) {
					$items[ $item_id ]['item_meta']['downloads'][] = sprintf( "<a href=\"%s\">%s</a>",
						//site_url(add_query_arg(array(
						//'dl_filename' => md5($key),
						//'item_id'	=> $item_id
						//))), 
						add_query_arg( [
							'dl_filename' => md5( $key ),
							'item_id'     => $item_id,
							'view-order'  => $order->id,
						], site_url( 'my-account' ) ),
						$name
					);
				} else {
					$items[ $item_id ]['item_meta']['downloads'][] = $name;
				}
			}
		}
		foreach ( $items as $item_id => $item ) {
			// bypass items without company_number set
			if ( ! array_key_exists( 'officer_number', $item ) || empty( $item['officer_number'] ) ) {
				continue;
			}
			// bypass processing for non downloadable orders
			if ( ! $order->is_download_permitted() ) {
				// show Nothing
				continue;
			}
			if ( ! array_key_exists( 'officer_details', $item ) || empty( $item['officer_details'] ) ) {
				$ch              = self::getConnector();
				$number          = base64_decode( $item['officer_number'] );
				$number          = $item['officer_number'];
				$officer_details = $ch->officerDetails( $number, 'example.com' );
				wc_update_order_item_meta( $item_id, '_officer_details', $officer_details );
				$items[ $item_id ]['officer_details'] = $officer_details;
			}
			$items[ $item_id ]['officer_details']          = maybe_unserialize( $items[ $item_id ]['officer_details'] );
			$filename                                      = md5( $item['officer_number'] );
			$items[ $item_id ]['item_meta']['downloads'][] = sprintf( "<a href=\"%s\">%s</a>",
				//site_url(add_query_arg(array(
				//'dlo_filename' => md5($item['officer_number']),
				//'item_id'	=> $item_id
				//))), 
				add_query_arg( [
					'dlo_filename' => md5( $item['officer_number'] ),
					'item_id'      => $item_id,
					'view-order'   => $order->id,
				], site_url( 'my-account' ) ),
				CP_Helper::getTranslation( "Download detailed information" )
			);
		}

		return $items;
	}

	public function generate_pdf_file( $details, $filename ) {
		// Include the main TCPDF library (search for installation path).
		require_once( CP_PLUGIN_PATH . '/lib/tcpdf/tcpdf.php' );

		// create new PDF document
		$pdf = new TCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false );

		$title = $subject = $keywords = '';
		// set document information
		$pdf->SetCreator( PDF_CREATOR );
		$pdf->SetAuthor( '' );
		$pdf->SetTitle( $title );
		$pdf->SetSubject( $subject );
		$pdf->SetKeywords( $keywords );

		// set default header data
		//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 021', PDF_HEADER_STRING);
		$pdf->SetHeaderData(
			CP_Helper::getTranslation( "../../Logo_OffshoreSU.jpeg" ) != "PDF_LOGO_FILE" ? CP_Helper::getTranslation( "../../Logo_OffshoreSU.jpeg" ) : '',
			//CP_Helper::getTranslation("PDF_LOGO_WIDTH"), 
			CP_Helper::getTranslation( "20" ),
			//CP_Helper::getTranslation("PDF_FILE_HEADER_TITLE"), 
			CP_Helper::getTranslation( "Записки об оффшорах" ),
			//CP_Helper::getTranslation("PDF_FILE_HEADER_ADDITIONAL_TEXT"));
			CP_Helper::getTranslation( "Оффшорные новости и практические оффшорные решения" ) );

		// set header and footer fonts
		//$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setHeaderFont( [ 'freeserif', '', 16 ] );
		$pdf->setFooterFont( [ PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA ] );

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont( PDF_FONT_MONOSPACED );

		// set margins
		$pdf->SetMargins( PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT );
		$pdf->SetHeaderMargin( PDF_MARGIN_HEADER );
		$pdf->SetFooterMargin( PDF_MARGIN_FOOTER );

		// set auto page breaks
		$pdf->SetAutoPageBreak( true, PDF_MARGIN_BOTTOM );

		// set image scale factor
		$pdf->setImageScale( PDF_IMAGE_SCALE_RATIO );

		// set some language-dependent strings (optional)
		if ( @file_exists( dirname( __FILE__ ) . '/lang/eng.php' ) ) {
			require_once( dirname( __FILE__ ) . '/lang/eng.php' );
			$pdf->setLanguageArray( $l );
		}

		// ---------------------------------------------------------

		// set default font subsetting mode
		$pdf->setFontSubsetting( true );
		// set font
		$pdf->SetFont( 'freeserif', '', 9 );

		// add a page
		$pdf->AddPage();

		$html = "<h1>";
		// create some HTML content
		$fields = [ 'honours', 'title', 'forename', 'surname' ];
		foreach ( $fields as $field ) {
			if ( array_key_exists( $field, $details ) && is_array( $details[ $field ] ) ) {
				$details[ $field ] = implode( " ", $details[ $field ] );
			};
			if ( ! empty( $details[ $field ] ) ) {
				$html .= $details[ $field ] . " ";
			};
		};
		$html .= "</h1>";

		if ( ! empty( $details['nationality'] ) ) {
			$html .= "" . CP_Helper::getTranslation( "Nationality: " ) . $details['nationality'] . "<br>";
		}
		if ( is_numeric( $details['dob'] ) ) {
			$html .= "" . CP_Helper::getTranslation( "DOB: " ) . date( get_option( 'date_format' ), $details['dob'] ) . "<br>";
		}
		// address data
		if ( array_key_exists( 'address', $details ) && is_array( $details['address'] ) ) {
			$html .= "<h4>" . CP_Helper::getTranslation( "Address:" ) . "</h4>";
			foreach ( $details['address'] as $line ) {
				if ( is_array( $line ) ) {
					$html .= implode( "<br>", $line ) . "<br>";
				} else {
					$html .= $line . "<br>";
				}
			}
		}
		// appointments info
		/*if (array_key_exists('appt_count', $details) && is_array($details['appt_count'])) {
			$html .= "<h4>".CP_Helper::getTranslation("Appointments Count")."</h4>";
			foreach ($details['appt_count'] as $key => $line) {
				$html .= CP_Helper::parseFieldName($key) . ': ' . $line . '<br>';
			};
		};*/
		// companies information and appointments
		if ( array_key_exists( 'company', $details ) && is_array( $details['company'] ) ) {
			$html .= "<h4>" . CP_Helper::getTranslation( "Companies" ) . "</h4>";
			$html .= "<table cellspacing='5' cellpadding='5' border='0'>";
			foreach ( $details['company'] as $company_number => $company ) {
				$html .= "<tr><td width='50%'>";
				foreach ( $company as $key => $value ) {
					if ( ! is_array( $value ) ) {
						$html .= $value . "<br>";
					} else {
						$html .= "</td><td width='50%'>";
						foreach ( $value as $appt ) {
							foreach ( $appt as $key2 => $value2 ) {
								$html .= CP_Helper::getTranslation( CP_Helper::parseFieldName( $key2 ) ) . ': ' . $this->parseDates( $key2, $value2 ) . '<br>';
							}
							$html .= "<br>";
						}
					}
				}
				$html .= "</td></tr>";
			}
			$html .= "</table>";
		}
		// output the HTML content
		$pdf->writeHTML( $html, true, 0, true, 0 );

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		//header('Content-Type: text/pdf');
		$pdf->Output( $filename, 'F' );
	}

	public function get_product_files( $item ) {
		$files = [];
		if ( array_key_exists( 'ch_documents', $item ) && ! empty( $item['ch_documents'] ) ) {
			if ( ! is_array( $item['ch_documents'] ) ) {
				$item['ch_documents'] = maybe_unserialize( $item['ch_documents'] );
				foreach ( $item['ch_documents'] as $key => $name ) {
					if ( array_key_exists( 'ch_files', $item ) && array_key_exists( $key, $item['ch_files'] ) && ! empty( $item['ch_files'][ $key ] ) ) {
						$url = $item['ch_files'][ $key ]['url'];
					} else {
						$url = $this->get_url_from_key( $item['company_number'], $item['company_name'], base64_decode( $key ) );
					};
					$name          = basename( $url );
					$hash          = md5( $url );
					$files[ $key ] = [
						'name' => $name,
						'url'  => $url,
					];
				}
			}
		}

		return $files;
	}

	public function get_url_from_key( $company_number, $company_name, $key ) {
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		require_once( ABSPATH . 'wp-admin/includes/media.php' );
		// local filename
		$attachment_url = '';
//echo $filename;
		$ch     = self::getConnector();
		$result = $ch->documentInfo( $company_number, $company_name, $key );
		//print_r($result);
		if ( is_array( $result ) && array_key_exists( 'key', $result ) ) {
			$doc_request_key = $result['key'];
			$result          = $ch->documentRequest( $doc_request_key, 'test ref' );
//print_r($result);
			if ( is_array( $result ) && array_key_exists( 'endpoint', $result ) ) {
				return $result['endpoint'];
			}
		}

		return $attachment_url;
	}

	public function download_ftp_file( $url ) {
		$tmpfname = wp_tempnam( $url );
		if ( ! $tmpfname ) {
			return new WP_Error( 'http_no_file', __( 'Could not create Temporary file.' ) );
		}
		file_put_contents( $tmpfname, file_get_contents( $url ) );

		return $tmpfname;
	}

	public function woocommerce_get_item_downloads( $files, $item, $order ) {
		if ( array_key_exists( 'downloads', $item['item_meta'] ) && ! empty( $item['item_meta']['downloads'] ) ) {
			foreach ( $item['item_meta']['downloads'] as $link ) {
				preg_match( "/<a href=\"(.*?)\">(.*?)<\/a>/is", $link, $m );
				$files[] = [
					'name'         => strip_tags( $m[2] ),
					'download_url' => $m[1],
				];
			}
		}
		//if ($item)
		//print_r($);
		return $files;
	}

	public function parse_request() {
		global $wp;
		if ( $wp->matched_query ) {
			parse_str( $wp->matched_query, $add_get );
			$_GET = array_merge( $add_get, $_GET );
		}
		if ( isset( $wp->query_vars['category_name'] ) ) {
			if ( preg_match( "/view-order\/(\d+)/is", $wp->query_vars['category_name'], $matches ) ) {
				$_GET['view-order'] = $matches[1];
			}
		}

		if ( isset( $_GET['remove_item'] ) && strpos( $_GET['remove_item'], ':' ) !== false ) {
			[ $company_number, $doc_key ] = explode( ':', $_GET['remove_item'], 2 );
			foreach ( WC()->cart->cart_contents as $cart_item_key => $cart_item_data ) {
				if ( array_key_exists( 'company_number', $cart_item_data ) && $cart_item_data['company_number'] == $company_number ) {
					if ( array_key_exists( 'ch-documents', $cart_item_data ) ) {
						if ( array_key_exists( $doc_key, $cart_item_data['ch-documents'] ) ) {
							$old_doc = WC()->cart->cart_contents[ $cart_item_key ]['ch-documents'][ $doc_key ];
							unset( WC()->cart->cart_contents[ $cart_item_key ]['ch-documents'][ $doc_key ] );
							if ( ! count( WC()->cart->cart_contents[ $cart_item_key ]['ch-documents'] ) ) {
								WC()->cart->remove_cart_item( $cart_item_key );
							}
							wc_add_notice( sprintf( __( '%s removed.', 'wp-companies-house' ), $old_doc ) );
							WC()->cart->set_session();
						}
					}
				}
			}
		}
		// process file download
		if ( isset( $_GET['dl_filename'] ) && isset( $_GET['item_id'] ) ) {
			if ( isset( $_GET['view-order'] ) ) {
				$order = new WC_Order( $_GET['view-order'] );
				foreach ( $order->get_items() as $item_id => $item ) {
					// we found needed item
					if ( $item_id == $_GET['item_id'] ) {
						//search for needed file
						foreach ( $item['ch_files'] as $key => $file ) {
							if ( $_GET['dl_filename'] == md5( $key ) ) {
								// we find needed file
								$filename = CP_PLUGIN_PATH . '/files/' . $file['name'];
								if ( ! file_exists( $filename ) ) {
									$contents = file_get_contents( $file['url'] );
									file_put_contents( $filename, $contents );
								}
								if ( file_exists( $filename ) ) {
									header( 'Content-Type: application/octet-stream' );
									header( "Content-Transfer-Encoding: Binary" );
									header( "Content-disposition: attachment; filename=\"" . basename( $filename ) . "\"" );
									readfile( $filename );
									die();
								}
							}
						}
					}
				}
			}
		}
		if ( isset( $_GET['dlo_filename'] ) && isset( $_GET['item_id'] ) ) {
			if ( isset( $_GET['view-order'] ) ) {
				$order = new WC_Order( $_GET['view-order'] );
				foreach ( $order->get_items() as $item_id => $item ) {
					// we found needed item
					if ( $item_id == $_GET['item_id'] ) {
						//search for needed file
						if ( $_GET['dlo_filename'] == md5( $item['officer_number'] ) ) {
							// we find needed file
							$filename = CP_PLUGIN_PATH . '/files/' . md5( $item['officer_number'] ) . '.pdf';
							if ( ! file_exists( $filename ) ) {
								$this->generate_pdf_file( $item['officer_details'], $filename );
							}
							if ( file_exists( $filename ) ) {
								header( 'Content-Type: application/octet-stream' );
								header( "Content-Transfer-Encoding: Binary" );
								header( "Content-disposition: attachment; filename=\"" . basename( $filename ) . "\"" );
								readfile( $filename );
								die();
							}
						}
					}
				}
			}
		}
	}

	public function woocommerce_add_order_item_meta( $item_id, $cart_item_data, $cart_item_key ) {
		if ( array_key_exists( 'company_number', $cart_item_data ) ) {
			wc_add_order_item_meta( $item_id, '_company_number', $cart_item_data['company_number'] );
			wc_add_order_item_meta( $item_id, '_company_name', $cart_item_data['company_name'] );
			wc_add_order_item_meta( $item_id, '_ch_documents', $cart_item_data['ch-documents'] );
			foreach ( $cart_item_data['ch-documents'] as $doc_name ) {
				//wc_add_order_item_meta($item_id, 'document', $doc_name);
			}
		}
		if ( array_key_exists( 'officer_number', $cart_item_data ) ) {
			wc_add_order_item_meta( $item_id, '_officer_number', $cart_item_data['officer_number'] );
			wc_add_order_item_meta( $item_id, '_officer_name', $cart_item_data['officer_name'] );
		}
	}

	public function woocommerce_checkout_update_order_review( $data ) {
		$this->woocommerce_before_cart_table();

		return $data;
	}
	/*
	 * Change price for each product
	 */
	//public function woocommerce_before_cart_table() {
	public function woocommerce_before_calculate_totals() {
		$cart = WC()->cart;
		foreach ( $cart->cart_contents as $cart_item_key => $cart_item_data ) {
			if ( $cart->cart_contents[ $cart_item_key ]['data']->id == CP_Helper::getOption( 'ch_xml_doc_product' ) ) {
				$product      = new WC_Product( CP_Helper::getOption( 'ch_xml_doc_product' ) );
				$plugin_price = $product->price;
				if ( ! array_key_exists( 'ch-documents', $cart->cart_contents[ $cart_item_key ] ) ) {
					$cart->cart_contents[ $cart_item_key ]['ch-documents'] = [];
				}
				$cart->cart_contents[ $cart_item_key ]['data']->price = count( $cart->cart_contents[ $cart_item_key ]['ch-documents'] ) * $plugin_price;
			}
		}
//print_r($cart->cart_contents);
	}

	/*
	 * Change sold_individually for shopping cart page to show QTY = 1 and DISALLOW to CHANGE quantity
	 */
	public function woocommerce_cart_item_product( $_product, $cart_item, $cart_item_key ) {
		$plugin_price = $_product->price;
		if ( $_product->id == CP_Helper::getOption( 'ch_xml_doc_product' ) ) {
			$_product->sold_individually = true;
		}
		if ( $_product->id == CP_Helper::getOption( 'ch_xml_off_product' ) ) {
			$_product->sold_individually = true;
		}

		return $_product;
	}

	/*
	 * Set ch-documents array
	 */
	public function woocommerce_add_cart_item( $cart_item_data, $cart_item_key ) {
		if ( array_key_exists( 'company_number', $cart_item_data ) ) {
			$docs = $_POST['document_name'];
			$keys = $_POST['document_key'];
			if ( ! is_array( $docs ) ) {
				$docs = [ $docs ];
				$keys = [ $keys ];
			};
			$company_name                   = $_POST['company_name'];
			$cart_item_data['company_name'] = $company_name;
			foreach ( $docs as $key => $doc ) {
				if ( ! array_key_exists( 'ch-documents', $cart_item_data ) ) {
					$cart_item_data['ch-documents'] = [];
				}
				$cart_item_data['ch-documents'][ stripslashes( $keys[ $key ] ) ] = stripslashes( $doc );
			}
		}
		if ( array_key_exists( 'officer_number', $cart_item_data ) ) {
			$cart_item_data['officer_name'] = $_POST['officer_name'];
		}

		return $cart_item_data;
	}

	/*
	 * Set only company number there - because only this data is different for our products
	 */
	public function woocommerce_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
		if ( array_key_exists( 'company_number', $_POST ) && ( $company_number = $_POST['company_number'] ) && ! empty( $company_number ) ) {
			$cart_item_data['company_number'] = $company_number;
		}
		if ( array_key_exists( 'officer_number', $_POST ) && ( $officer_number = $_POST['officer_number'] ) && ! empty( $officer_number ) ) {
			$cart_item_data['officer_number'] = $officer_number;
		}

//print_r($cart_item_data);
		return $cart_item_data;
	}

	/*
	 * Set quantity to be always 1 - price will be changed based on documents requested
	 */
	public function woocommerce_after_cart_item_quantity_update( $cart_item_key, $nqty, $oqty ) {
		$cart = WC()->cart;
		if ( $cart->cart_contents[ $cart_item_key ]['product_id'] == CP_Helper::getOption( 'ch_xml_doc_product' ) ) {
			$cart->cart_contents[ $cart_item_key ]['quantity'] = 1;
		}
		if ( $cart->cart_contents[ $cart_item_key ]['product_id'] == CP_Helper::getOption( 'ch_xml_off_product' ) ) {
			$cart->cart_contents[ $cart_item_key ]['quantity'] = 1;
		}
		// do add function because qty update - no way to add existing products attributes. only when QTY update
		$cart->cart_contents[ $cart_item_key ] = $this->woocommerce_add_cart_item( $cart->cart_contents[ $cart_item_key ], $cart_item_key );
	}

	public function woocommerce_item_name( $title, $cart_item, $cart_item_key = null ) {
		if ( array_key_exists( 'company_name', $cart_item ) && ! empty( $cart_item['company_name'] ) ) {
			$link = add_query_arg( 'company_number', $cart_item['company_number'], get_permalink( CP_Helper::getPageId( 'company' ) ) );

			return sprintf( '<a href="%s">%s</a>', $link, $cart_item['company_name'] );
		}
		if ( array_key_exists( 'officer_name', $cart_item ) && ! empty( $cart_item['officer_name'] ) ) {
			return sprintf( "Officer details:<br /> %s", $cart_item['officer_name'] );
		}

		return $title;
	}

	public function woocommerce_item_data( $data, $cart_item ) {
		//print_r($cart_item);
//print_r($cart_item);
		if ( array_key_exists( 'ch-documents', $cart_item ) && ! empty( $cart_item['ch-documents'] ) ) {
//print_r($cart_item['ch-documents']);
			foreach ( $cart_item['ch-documents'] as $key => $name ) {
				[ $k, $v ] = explode( ':', $name, 2 );
				$link = '';
				if ( get_option( 'woocommerce_cart_page_id' ) == get_query_var( 'page_id' ) ) {
					$link = sprintf( '<a style="%s" class="remove" href="%s" title="%s">&times;</a>', 'display: inline;', esc_url( WC()->cart->get_remove_url( $cart_item['company_number'] . ':' . rawurlencode( $key ) ) ), __( 'Remove this item', 'woocommerce' ) );
				}
				$data[] = [
					'name'  => $link . $k,
					'value' => $v,
				];
			}
		}

		return $data;
	}

	static function getConnector() {
		static $_connector = null;
		if ( is_null( $_connector ) ) {
			$_connector   = new CompaniesHousePanama( CP_Helper::getOption( 'ch_xml_username' ), CP_Helper::getOption( 'ch_xml_password' ) );
			$request_info = CP_Helper::getOption( 'ch_xml_email' );
			if ( ! empty( $request_info ) ) {
				$_connector->setSenderEmailAddress( $request_info );
			}
		}

		return $_connector;
	}

	public function getContent( $content ) {
		if ( ! is_null( $this->post_content ) ) {
			$content = $this->post_content;
		}

		return $content;
	}

	public function getTitle( $content ) {
		if ( ! is_null( $this->post_title ) ) {
			$content = $this->post_title;
		}

		return $content;
	}

	public function seoTitle( $content ) {
		if ( ! is_null( $this->post_title ) ) {
			$content = str_replace( '%post_title%', $this->post_title, $content );
		}

		return $content;
	}

	public function showCompanyPagePanama( $post ) {
		if ( ! CP_Helper::isCompanyPage( $post ) ) {
			return $post;
		}
		$post_processed = false;
		switch ( true ) {
			/*
						case (array_key_exists('company_doc', $_GET) && !empty($_GET['company_doc'])):
							$ch = self::getConnector();
							$company_number = $_GET['company_number'];
							$doc_key = base64_decode($_GET['company_doc']);
							// get company Name
							$res = $ch->companyDetails($company_number, false);
							$doc_info = array('company_name'	=> $res['name']);
							$company_name = $res['name'];
							// get document description
							$res = $ch->filingHistory($company_number);
							foreach ($res as $doc) {
								if (isset($doc['key']) && $doc['key'] == $doc_key) $doc_info['description'] = implode("<br />", $doc['description']);
							};
							$post->post_title = CP_Helper::getTranslation("Document info");
							$result = $ch->documentInfo($company_number, $company_name, $doc_key);
							$doc_info = array_merge($doc_info, $result);
							$doc_info['doc_key'] = $doc_key;
							$post->post_content .= CP_Helper::getTemplate('document-info.php', array(
								'companyNumber'	=> $company_number,
								'companyName'	=> $company_name,
								'document'		=> $doc_info
							));
							$post_processed = true;
							break;
			*/
			case ( array_key_exists( 'show_documents', $_GET ) && ! empty( $_GET['show_documents'] ) ):
				$ch             = self::getConnector();
				$company_number = $_GET['company_number'];
				// get company Name
				$res                = $ch->companyDetails( $company_number, false );
				$company_name       = $res['name'];
				$post->post_title   = sprintf( CP_Helper::getTranslation( "Documents for '%s'" ), $company_name );
				$result             = $ch->filingHistory( $company_number );
				$post->post_content .= CP_Helper::getTemplate( 'companies-search-documents.php', [
					'company_number' => $company_number,
					'company_name'   => $company_name,
					'documents'      => $result,
				] );
				$post_processed     = true;
				break;
			case ( array_key_exists( 'search_company', $_GET ) && ! empty( $_GET['search_company'] ) ):
				$ch               = self::getConnector();
				$name             = $_GET['search_company'];
				$post->post_title = '';
				CP_Helper::getTranslation( "Companies search result " );
				$post->post_content .= CP_Helper::getTemplate( 'companies-search-result.php', [
					'name'      => $name,
					'companies' => $ch->companyNameSearch( $name ),
				] );
				$post_processed     = true;
				break;
			case ( array_key_exists( 'company_number', $_GET ) && ! empty( $_GET['company_number'] ) ):
				$ch               = self::getConnector();
				$number           = $_GET['company_number'];
				$post->post_title = CP_Helper::getTranslation( "Company details" );
				$company_details  = $this->parseDetails( $ch->companyDetails( $number, CP_Helper::getOption( 'ch_xml_morttotals' ) == 1 ) );
// disable Filling link
				//$company_details['Workers'] = "<a href=\"".add_query_arg('show_workers', 1)."\">".__("Show workers")."</a>";
				//$company_details['Filling History'] = "<a href=\"".add_query_arg('show_documents', 1)."\">".__("Filing History")."</a>";
				$post->post_content .= CP_Helper::getTemplate( 'companies-search-details.php', [
					'details' => $company_details,
				] );
				$post_processed     = true;
				break;
			case ( array_key_exists( 'anumber', $_GET ) && ! empty( $_GET['anumber'] ) ):
				$ch               = self::getConnector();
				$number           = $_GET['anumber'];
				$cname            = $_GET['cname'];
				$post->post_title = CP_Helper::getTranslation( "Company Appointments details" );
				$resigned         = ( CP_Helper::getOption( 'ch_xml_appresigned' ) == 1 );
				$result           = $ch->companyAppointmentsDetails( $cname, $number, $resigned );
				$company_details  = $this->parseDetails( $result );
				/*$post->post_content .= CP_Helper::getTemplate('appointments-search-details.php', array(
					'details'	=> $company_details
				));*/
//print_r($company_details);
				$post_processed = true;
				break;
			default:
				$post->post_title   = CP_Helper::getTranslation( "Companies search" );
				$post->post_content .= CP_Helper::getTemplate( 'companies-search-form.php' );
				$post_processed     = true;
				break;
		}
		if ( $post_processed === true ) {
			$this->post_content = $post->post_content;
			$this->post_title   = $post->post_title;
		}

		return $post;
	}

	public function showOfficerPagePanama( $post ) {
		if ( ! CP_Helper::isOfficerPage( $post ) ) {
			return $post;
		}
		$post_processed = false;
		switch ( true ) {
			case ( array_key_exists( 'officer_id', $_GET ) && ! empty( $_GET['officer_id'] ) ):
				$ch                 = self::getConnector();
				$number             = base64_decode( $_GET['officer_id'] );
				$post->post_title   = CP_Helper::getTranslation( "Officer details" );
				$company_details    = $ch->officerDetails( $number, 'example.com' );
				$company_details    = $this->parseOfficerDetails( $company_details );
				$post->post_content .= CP_Helper::getTemplate( 'officer-search-details.php', [
					'details' => $company_details,
				] );
				$post_processed     = true;
				break;
			case ( array_key_exists( 'search_officer', $_GET ) && ! empty( $_GET['search_officer'] ) ):
				$ch                 = self::getConnector();
				$name               = $_GET['search_officer'];
				$offtype            = CP_Helper::getOption( 'ch_xml_offtype' );
				$offresigned        = CP_Helper::getOption( 'ch_xml_offresigned' );
				$post->post_content .= CP_Helper::getTemplate( 'officer-search-result.php', [
					'name'     => $name,
					'officers' => $ch->companyOfficerSearch( $name, null, $offtype, null, $offresigned ),
				] );
				$post->post_title   = CP_Helper::getTranslation( "Officer search result" );
				$post_processed     = true;
				break;
			default:
				$post->post_title   = CP_Helper::getTranslation( "Officer search" );
				$post->post_content .= CP_Helper::getTemplate( 'officer-search-form.php' );
				$post_processed     = true;
				break;
		}
		if ( $post_processed === true ) {
			$this->post_content = $post->post_content;
			$this->post_title   = $post->post_title;
		}

		return $post;
	}

	public function parseOfficerDetails( $details ) {
		$new_details                 = [];
		$nums                        = [];
		$new_details['officer_name'] = "{$details['forename']} {$details['surname']}";
		foreach ( $details['company'] as $id => $info ) {
			$link                                                                       = add_query_arg( 'company_number', $info['number'], get_permalink( CP_Helper::getPageId( 'company_panama' ) ) );
			$new_details[ $info['number'] ][ $this->parseDetailName( 'company_name' ) ] = "<a href='$link' class='ch-company-details-link'>{$info['name']}</a>";
			//$new_details[$id+1]['company_status'] = $info['status'];
			$nums[ $info['number'] ] ++;
			$new_details[ $info['number'] ][ $this->parseDetailName( 'appoimtment_type' ) ][ $nums[ $info['number'] ] ] = $info['appoimtment_type'];
		}

		return $this->parseDetails( $new_details );
	}

	public function parseDetails( $details ) {
		$new_details = [];
		if ( empty( $details ) ) {
			$details = [];
		}
		foreach ( $details as $name => $value ) {
			if ( in_array( $name, [ 'address', 'sic_code' ] ) ) {
				$value = implode( '<br />', array_filter( $value ) );
				$value = trim( $value, " \r\n\t" );
				$value = preg_replace( "/^(\s*<br \/>)/si", "", $value );
				$value = preg_replace( "/(<br \/>)$/si", "", $value );
				$value = preg_replace( "/(<br \/><br \/>)/i", "<br />", $value );
			}
			/*if ($name == 'appointments' || $name == 'appointment') {
				if (!is_array($value)) {
					// disable appointments link
					//continue;
					$link = add_query_arg(array(
							'anumber'	=> $_GET['company_number'],
							'cname'		=> $details['name'],
					), remove_query_arg('company_number'));
						
					if ($value > 0) {
						//$value = " <a href='$link' class='appoinments-link'>" . CP_Helper::getTranslation('(View appointments)') . "</a>";
						$ch = self::getConnector();
						$result = $ch->companyAppointmentsDetails('', $_GET['company_number'], 0);
						$company_details = $this->parseDetails($result);
						$value = CP_Helper::getTemplate('appointments-search-details.php', array(
							'details'	=> $company_details
						));
						
						//exit;
					}
					else $value = '';
				} else {
					//$new_details[$this->parseDetailName($name)] = $value;
				};
			};*/
			// ignore empty rows
			if ( empty( $value ) ) {
				continue;
			}
			// ignore 
			//if ($name == 'person_id') continue;
			if ( $name == 'continuation_key' ) {
				continue;
			}
			//if ($name == 'forename') continue;
			if ( $name == 'forename' ) {
				$new_details[ $this->parseDetailName( $name ) ] = $value;
				$name                                           = 'name';
				$link                                           = get_permalink( CP_Helper::getPageId( 'officer_panama' ) );
				$link                                           = add_query_arg( 'officer_id', base64_encode( $details['person_id'] ), $link );
				$value                                          = "<a href='$link' class='ap-details-link'>{$details['forename']} {$details['surname']}</a>";
				//$value = "{$details['forename']} {$details['surname']}";
			}
			$value = $this->parseDates( $name, $value );

			if ( is_array( $value ) ) {//print_r($value);
				foreach ( $value as $name2 => $value2 ) {
					if ( empty( $value2 ) ) {
						continue;
					}
					// temporary bypass document information
					if ( $name2 && $name2 == 'document' ) {
						continue;
					} // Strange fix: had to add $name2 && as when it was zero got triggered!
					$new_value = $this->parseDates( $name2, $value2 );

					if ( ! preg_match( '#^[0-9]+$#si', $name ) ) {
						$new_name = $this->parseDetailName( $name . '_' . $name2 );
					} else {
						$new_name = CP_Helper::getTranslation( $this->parseDetailName( $name2 ) ) . ' #' . ( $i[ $name2 ] + 1 );
					}
					$i[ $name2 ] ++;
					if ( $name == 'appointments' || $name == 'appointment' ) {
						$new_name = CP_Helper::getTranslation( $this->parseDetailName( substr( $name, 0, - 1 ) ) ) . ' #' . ( $name2 + 1 );
						if ( is_array( $new_value ) ) {
							$new_value = $this->parseDetails( $new_value );
						}//print_r($new_value);
					};
					$new_details[ $new_name ] = $new_value;

					if ( $name == 'company' ) {
						//print_r($new_details);
					}
				}
			} else {
				$new_name = $this->parseDetailName( $name );
				if ( $name == 'dob' ) {
					$new_name = 'DOB';
				}

				$new_details[ $new_name ] = $value;
			}
		}

		return $new_details;
	}

	public function parseDates( $name, $value ) {
		if ( in_array( $name, [
				'resignation',
				'dissolution_date',
				'registration_date',
				'incorporation_date',
				'due_date',
				'last_madeup',
				'date',
			] ) && ! is_array( $value ) && is_numeric( $value ) ) {
			return date( get_option( 'date_format' ), $value );
		}

		return $value;
	}

	public function parseDetailName( $name ) {
		return ucwords( str_replace( "_", " ", $name ) );
	}
}
