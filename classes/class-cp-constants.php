<?php
	class CP_Constants {
		static function getConstant($name) {
			static $constants = array(
				'pages'	=> array(
					'company_panama'	=> array (
						'title'		=> 'Companies House search'
					),
					'officer_panama'	=> array(
						'title'		=> 'Panama Officer search'
					)
				)
			);
			if (array_key_exists($name, $constants)) {
				return $constants[$name];
			};
			return false;
		}
	}
?>
