<?php

#
#  CompaniesHouse.php
#
#  Created by Jonathon Wardman on 20-07-2009.
#  Copyright 2009, Fubra Limited. All rights reserved.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  You may obtain a copy of the License at:
#  http://www.gnu.org/licenses/gpl-3.0.txt
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  This is a modified version of Jonathon Wardman's orignal CompaniesHouse.php
#  file that has extra/altered methods that may not exist in the orignal file. This
#  comment has been added to conform with the GNU GPL licence. Modifications
#  by Alex Sansom.

/**
 * Companies House API client.  Extends the functionality provided by the
 * GovTalk class to build and parse Companies House data.  The php-govtalk
 * base class needs including externally in order to use this extention.
 *
 * @author Jonathon Wardman
 * @copyright 2009, Fubra Limited
 * @licence http://www.gnu.org/licenses/gpl-3.0.txt GNU General Public License
 */
class CompaniesHousePanama extends GovTalk {

 /* System / internal variables. */

	/**
	 * Version of the extension, for use in channel routing.
	 *
	 * @var string
	 */
	private $_extensionVersion = '0.3.1';

 /* Magic methods. */

	/**
	 * Instance constructor. Contains a hard-coded CH XMLGW URL and additional
	 * schema location.  Adds a channel route identifying the use of this
	 * extension.
	 *
	 * @param string $govTalkSenderId GovTalk sender ID.
	 * @param string $govTalkPassword GovTalk password.
	 */
	public function __construct($govTalkSenderId, $govTalkPassword) {
	
		parent::__construct('http://xmlgw.companieshouse.gov.uk/v1-0/xmlgw/Gateway', $govTalkSenderId, $govTalkPassword);
		$this->setSchemaLocation('http://xmlgw.companieshouse.gov.uk/v1-0/schema/Egov_ch-v2-0.xsd');
		$this->setSchemaValidation(false);
		$this->setMessageAuthentication('alternative');
		$this->setMessageQualifier('request');

	}

 /* Public methods. */

 /* Search methods. */
 
	/**
	 * Searches for companies with a registered name matching or similar to the
	 * given company name. Processes a company NameSearch and returns the results.
	 *
	 * @param string $companyName The name of the company for which to search.
	 * @param string $dataset The dataset to search within ('LIVE', 'DISSOLVED', 'FORMER', 'PROPOSED').
	 * @return mixed An array of companies found by the search, or false on failure.
	 */
	public function companyNameSearch($companyName, $dataset = 'LIVE') {
	
		if (($companyName != '') && (strlen($companyName) < 161)) {
			$dataset = strtoupper($dataset);
			switch ($dataset) {
			   case 'LIVE': case 'DISSOLVED': case 'FORMER': case 'PROPOSED':
			   
					global $wpdb;
					
					// country detection
					$sql = "SELECT * FROM companyHouse_country";
					$countries_db = $wpdb->get_results( $sql );// var_dump($countries_db);
					if ($countries_db == null || !is_array($countries_db)) 
						return false;
					$countries = array();
					foreach ($countries_db as $c) {
						if (stristr($companyName, $c->country))
							$countries[] = $c->id;
							//$companyName = str_ireplace($c->country, '', $companyName);
					}
					if (!empty($countries)) {
						$countries = 'AND `RegAddress.Country` IN (' . implode(',', $countries) . '/*, 0*/)';
					} else {
						$countries = '';
					}
					$countries = '';
					$companyName = strtoupper($companyName);
					$companyName = '+"'.preg_replace('/[\s,]+\s*/i', '" +"', $companyName).'"';
					//$sql = "SELECT * FROM companyHouse where CompanyName LIKE '%$companyName%' OR `RegAddress.AddressLine1` LIKE '%$companyName%' OR `RegAddress.AddressLine2` LIKE '%$companyName%' OR `RegAddress.PostTown` LIKE '%$companyName%' OR `RegAddress.County` LIKE '%$companyName%' OR `RegAddress.Country` LIKE '%$companyName%' LIMIT 100";
					$sql = "SELECT * FROM companyHouse where MATCH (`RegAddress.AddressLine1`, `RegAddress.AddressLine2`,  `RegAddress.PostTown`,  `RegAddress.PostCode`) AGAINST ( '$companyName' IN BOOLEAN MODE) $countries LIMIT 100";
					$companySearchBody = $wpdb->get_results( $sql );
					
					$sql_tot = "SELECT COUNT(*) as total FROM companyHouse where MATCH (`RegAddress.AddressLine1`, `RegAddress.AddressLine2`,  `RegAddress.PostTown`,  `RegAddress.PostCode`) AGAINST ( '$companyName' IN BOOLEAN MODE ) $countries ";
					$companySearchTotoal = $wpdb->get_results( $sql_tot );
					//var_dump($companySearchTotoal);exit;
					
					//var_dump($companySearchBody);exit;
					$exactCompany = $possibleCompanies = array();

						if ($companySearchBody !== null && is_array($companySearchBody)) {		
									
			foreach ($companySearchBody AS $possibleCompany) {
				$thisCompanyDetails = array('name' => (string) $possibleCompany->CompanyName,
				                            'number' => (string) $possibleCompany->CompanyNumber,/*id*/
				                            'address' => (string) $possibleCompany->{'RegAddress.AddressLine1'} . ' ' . $possibleCompany->{'RegAddress.AddressLine2'} . ', ' . $possibleCompany->{'RegAddress.PostTown'} /*. ', ' . $possibleCompany->{'RegAddress.Country'}*/ . ', ' . $possibleCompany->{'RegAddress.PostCode'}, 
				                            'status' => (string) $possibleCompany->CompanyStatus
				                            );
				$possibleCompanies[] = $thisCompanyDetails;
				if (isset($possibleCompany->SearchMatch) && ((string) $possibleCompany->SearchMatch == 'EXACT')) {
					$exactCompany = $thisCompanyDetails;
				}
			}
			return array('exact' => $exactCompany,
			             'match' => $possibleCompanies,
						 'total' => $companySearchTotoal[0]->total);
					
					}
					
					return false;
			   
					$this->setMessageClass('NameSearch');

					$package = new XMLWriter();
					$package->openMemory();
					$package->setIndent(true);
					$package->startElement('NameSearchRequest');
						$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/NameSearch.xsd');
						$package->writeElement('CompanyName', $companyName);
						$package->writeElement('DataSet', $dataset);
					$package->endElement();

					$this->setMessageBody($package);
					$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
					if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
						return $this->_parseCompanySearchResult($this->getResponseBody()->NameSearch);
					} else {
						return false;
					}
					
			   break;
			   default:
				   return false;
				break;
			}
		} else {
			return false;
		}
	
	}
	// get filling history
	public function getCompanyDocuments($companyNumber, $withCapital = false) {
		if (preg_match('/[A-Z0-9]{1,8}[*]{0,1}/', $companyNumber)) {

			$this->setMessageClass('NumberSearch');

			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('FilingHistoryRequest');
				$package->writeAttribute('xsi:schemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema http://xmlgw.companieshouse.gov.uk/v1-0/schema/FilingHistory-v2-1.xsd');
				$package->writeElement('CompanyNumber', $companyNumber);
				$package->writeElement('CapitalDocInd', $withCapital);
			$package->endElement();

			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
				return $this->_parseCompanyDocumentsResult($this->getResponseBody()->NumberSearch);
			} else {
				return false;
			}
		};
		return false;
	}
	
	/**
	 * Searches for companies matching the given company number.  Processes a
	 * company NumberSearch and returns the results.
	 *
	 * @param string $companyNumber The number (or partial number) of the company for which to search.
	 * @param string $dataset The dataset to search within ('LIVE', 'DISSOLVED', 'FORMER', 'PROPOSED').
	 * @return mixed An array of companies found by the search, or false on failure.
	 */
	public function companyNumberSearch($companyNumber, $dataset = 'LIVE') {

		if (preg_match('/[A-Z0-9]{1,8}[*]{0,1}/', $companyNumber)) {
			$dataset = strtoupper($dataset);
			switch ($dataset) {
			   case 'LIVE': case 'DISSOLVED': case 'FORMER': case 'PROPOSED':

					$this->setMessageClass('NumberSearch');

					$package = new XMLWriter();
					$package->openMemory();
					$package->setIndent(true);
					$package->startElement('NumberSearchRequest');
						$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/NumberSearch.xsd');
						$package->writeElement('PartialCompanyNumber', $companyNumber);
						$package->writeElement('DataSet', $dataset);
					$package->endElement();

					$this->setMessageBody($package);
					$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
					if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
						return $this->_parseCompanySearchResult($this->getResponseBody()->NumberSearch);
					} else {
						return false;
					}

			   break;
			   default:
				   return false;
				break;
			}
		} else {
			return false;
		}

	}
	
	/**
	 * Searches for company officers matching the given criterion. Processes a
	 * company OfficerSearch and returns the results.
	 *
	 * The officer search may be restricted by officer type by passing one of the
	 * following as the third argument:
	 *   * DIS - Disqualified directors.
	 *   * LLP - Limited Liability partnerships.
	 *   * CUR - None of the above (default).
	 *   * EUR - SE and ES apointments only.
	 *
	 * @param string $officerSurname The surname of the officer for which to search.
	 * @param mixed $officerForename The forename(s) of the officer for which to search. If an array is passed all forenames will be used.
	 * @param string $officerType The type of officer for which to search (CUR, LLP, DIS, EUR).
	 * @param string $postTown The post town of the officer for which to search.
	 * @param boolean $includeResigned If $officerType == CUR, then indicate whther to include only directors that hold current appointments.
	 * @return mixed An array 'exact' => the match marked as nearest by CH, 'match' => all matches returned by CH, or false on failure.
	 */
	function companyOfficerSearch($officerSurname, $officerForename = null, $officerType = 'CUR', $postTown = null, $includeResigned = 0) {
	
		if ($officerSurname != '') {
			switch ($officerType) {
				case 'DIS': case 'LLP': case 'CUR': case 'EUR':

					global $wpdb;
					$officerSearchBody = $wpdb->get_results( "SELECT * FROM person where name LIKE '%$officerSurname%' LIMIT 100" );
					//var_dump($myrows);exit;
					$nearestOfficer = $possibleOfficers = array();

						if ($officerSearchBody !== null) {
							if (is_array($officerSearchBody)) {

								foreach ($officerSearchBody AS $officerDetails) {
								
									$thisOfficerDetails = array('id' => (string) $officerDetails->id,
							                            'title' => str_replace(',', '', (string) ''/*$officerDetails->Title*/),
							                            'surname' => (string) $officerDetails->name,
							                            'forename' => (string) ''/*$officerDetails->Forename*/,
							                            'dob' => strtotime((string) ''/*$officerDetails->DOB*/),
							                            'posttown' => (string) ''/*$officerDetails->PostTown*/,
							                            'postcode' => rtrim((string) ''/*$officerDetails->Postcode*/)
                                                                            );
                                                                            
                                     $possibleOfficers[] = $thisOfficerDetails; 

								
								}
								
						return array(
                                                        'search_criteria' => $search_criteria,
                                                        'continuation_key' => (string) ''/*$officerSearchBody->ContinuationKey*/,
                                                        'search_rows' => (string) count($officerSearchBody)/*->SearchRows*/,
                                                        'exact' => $nearestOfficer,
						        'match' => $possibleOfficers
                                                    );
								
                             }
                         }


return false;

					$this->setMessageClass('OfficerSearch');
					
					$package = new XMLWriter();
					$package->openMemory();
					$package->setIndent(true);
					$package->startElement('OfficerSearchRequest');
						$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/OfficerSearch.xsd');
						$package->writeElement('Surname', $officerSurname);
						$package->writeElement('OfficerType', $officerType);
						
						
						if ($officerForename !== null) {
							if (is_array($officerForename)) {
								$forenameCount = 0;
								foreach ($officerForename AS $singleForename) {
									$package->writeElement('Forename', $singleForename);
									if (++$forenameCount == 2) {
										break;
									}
								}
							} else {
								$package->writeElement('Forename', $officerForename);
							}
						}
						if ($postTown !== null) {
							$package->writeElement('PostTown', $postTown);
						}
						if ($officerType == 'CUR') {
							$package->writeElement('IncludeResignedInd', $includeResigned);
						}
					$package->endElement();
					
					$this->setMessageBody($package);
					$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
					if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
						$nearestOfficer = $possibleOfficers = array();
						$officerSearchBody = $this->getResponseBody()->OfficerSearch;
						foreach ($officerSearchBody->OfficerSearchItem AS $officerDetails) {
                                                    
							$thisOfficerDetails = array('id' => (string) $officerDetails->PersonID,
							                            'title' => str_replace(',', '', (string) $officerDetails->Title),
							                            'surname' => (string) $officerDetails->Surname,
							                            'forename' => (string) $officerDetails->Forename,
							                            'dob' => strtotime((string) $officerDetails->DOB),
							                            'posttown' => (string) $officerDetails->PostTown,
							                            'postcode' => rtrim((string) $officerDetails->Postcode)
                                                                            );

                                                        if (isset($officerDetails->CountryOfResidence)) {
                                                            $thisOfficerDetails['country_of_residence'] = (string) $officerDetails->CountryOfResidence;
                                                        }

                                                        if (isset($officerDetails->StateOfResidence)) {
                                                            $thisOfficerDetails['state_of_residence'] = (string) $officerDetails->StateOfResidence;
                                                        }

                                                        // AS: This potentially records the director info history. There's no PersonID element for each DuplicateOfficerSearchItem.
                                                        // Not all elements are necessarily present.
                                                        if (isset($officerDetails->DuplicateOfficers)) {
                                                            foreach ($officerDetails->DuplicateOfficers->DuplicateOfficerSearchItem AS $duplicate) {
                                                                $duplicate_officer_details = array();

                                                                if (isset($duplicate->Title)) {
                                                                    $duplicate_officer_details['title'] = str_replace(',', '', (string) $duplicate->Title);
                                                                }

                                                                if (isset($duplicate->Surname)) {
                                                                    $duplicate_officer_details['surname'] = (string) $duplicate->Surname;
                                                                }

                                                                if (isset($duplicate->Forename)) {
                                                                    $duplicate_officer_details['forename'] = (string) $duplicate->Forename;
                                                                }

                                                                if (isset($duplicate->DOB)) {
                                                                    $duplicate_officer_details['dob'] = (string) $duplicate->DOB;
                                                                }

                                                                if (isset($duplicate->PostTown)) {
                                                                    $duplicate_officer_details['posttown'] = (string) $duplicate->PostTown;
                                                                }

                                                                if (isset($duplicate->Postcode)) {
                                                                    $duplicate_officer_details['postcode'] = (string) $duplicate->Postcode;
                                                                }

                                                                if (isset($duplicate->CountryOfResidence)) {
                                                                    $duplicate_officer_details['country_of_residence'] = (string) $duplicate->CountryOfResidence;
                                                                }

                                                                $thisOfficerDetails['duplicate_officers'][] = $duplicate_officer_details;
                                                            }
                                                        }

                                                        if (isset($officerDetails->SearchMatch) && ((string) $officerDetails->SearchMatch == 'NEAR')) {
                                                            $nearestOfficer = $thisOfficerDetails;
							} else {
                                                            $possibleOfficers[] = $thisOfficerDetails;
                                                        }
						}
						return array(
                                                        'search_criteria' => $search_criteria,
                                                        'continuation_key' => (string) $officerSearchBody->ContinuationKey,
                                                        'search_rows' => (string) $officerSearchBody->SearchRows,
                                                        'exact' => $nearestOfficer,
						        'match' => $possibleOfficers
                                                    );
					} else {
						return false;
					}
					
				break;
				default:
					return false;
				break;
			}
		} else {
			return false;
		}
	
	}

        /*
         * AS: Addition
         *
         * @param string $companyName. Name of company appointments are required for.
         * @param string $companyNumber. Companies House number appointments are required for.
         * @param boolean $includeResigned. Boolean flag to indicate whether to retoreve more than current oompany appointments.
         * @return mixed An array of info about all the appointed company persons OR false.
         */
        function companyAppointmentsDetails($companyName, $companyNumber, $includeResigned = false) {
            if (preg_match('/[0-9]+/', $companyNumber)) {//[A-Z0-9]{8,8}
                
                $this->setMessageClass('CompanyAppointments');




					global $wpdb;
					$companyApptsBody = $wpdb->get_results( "SELECT * FROM link, company, person where person.id=link.person_id AND  link.company_id=company.id AND company.recordid = '$companyNumber' " );//GROUP BY person_id
					//var_dump($companyDetailsBody);exit;

						if ($companyApptsBody !== null) {
							if (is_array($companyApptsBody) ) {
							
							
                            $companyAppointments = array();
                            $i = 0;

                            //$companyAppointments['company_name'] = (string) $companyName/*$companyApptsBody->name*/;
                            //$companyAppointments['company_number'] =  (string) $companyApptsBody[0]->recordid;
                            /*$companyAppointments['has_inconsistencies'] = 1;//(string) $companyApptsBody->HasInconsistencies;
                            $companyAppointments['num_current_appt'] = 1;//(string) $companyApptsBody->NumCurrentAppt;
                            $companyAppointments['num_resigned_appt'] = 1;//(string) $companyApptsBody-> NumResignedAppt;
                            $companyAppointments['search_rows'] = 1;//(string) $companyApptsBody->SearchRows; 
                            $companyAppointments['continuation_key'] = 1;//(string) $companyApptsBody->ContinuationKey;/**/
                            
//$companyApptsBody[1] = $companyApptsBody[0];

                            foreach ($companyApptsBody AS $person) {
                                /*$person = $appointment->Person;
                                $address = $person->PersonAddress;*/

                                $thisAppointmentDetails = array(
                                    'forename' => (string) $person->name,
                                    'surname' => ' ',//(string) $person->Surname,
                                    //'title' => 1,//(string) $person->Title,
                                    /*'honours' => 1,//(string) $person->Honours,
                                    'dob' => 1,//(string) $person->DOB,
                                    'nationality' => 1,//(string) $person->Nationality,
                                    'country_of_residence' => 1,//(string) $person->CountryOfResidence,
                                    'address' => array(
                                        'care_of' => 1,//(string) $address->CareOfName,
                                        'po_box' => 1,//(string) $address->POBox,
                                        'address_line' => 1,//(string) $address->AddressLine,
                                        'post_town' => 1,//(string) $address->PostTown,
					'county' => 1,//(string) $address->County,
					'country' => 1,//(string) $address->Country,
					'postcode' => 1,//(string) $address->Postcode
                                    ),*/
                                    'person_id' => (string) $person->person_id,//$person->id,
                                    /*'number_of_appointments' => 1,//(string) $appointment->NumAppointments,
                                    'num_app_ind' => 1,//(string) $appointment->NumAppInd,*/
                                    'appoimtment_type' => (string) $person->role,
                                    /*'appointment_status' => 1,//(string) $appointment->AppointmentStatus,
                                    'appointment_date_prefix' => 1,//(string) $appointment->ApptDatePrefix,
                                    'appointment_date' => 1,//(string) $appointment->AppointmentDate,
                                    'resignation_date' => 1,//(string) $appointment->ResignationDate,
                                    'occupation' => 1,//(string) $appointment->Occupation,*/
                                );
                                $companyAppointments['appointments'][$i] = $thisAppointmentDetails;
                                $i++;
                            }
//var_dump($companyAppointments);exit;
                            return $companyAppointments;                            
                            
                            
								
                             }
                         }


return false;


                $package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('CompanyApptRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/CompanyAppointments.xsd');
                                $package->writeElement('CompanyName', $companyName);
				$package->writeElement('CompanyNumber', $companyNumber);
				if ($includeResigned === true) {
					$package->writeElement('IncludeResignedInd', '1');
				}
                                // AS: UserReference is inc in the example.
                                $package->writeElement('UserReference', '');
                                // AS: ContinuationKey is NOT inc in the example. minOccurs="0"
                                // $package->writeElement('ContinuationKey', '');
			$package->endElement();

			$this->setMessageBody($package);

                        if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
                            
                            $companyApptsBody = $this->getResponseBody()->CompanyAppointments;
                            $companyAppointments = array();
                            $i = 0;

                            $companyAppointments['company_name'] = (string) $companyApptsBody->CompanyName;
                            $companyAppointments['company_number'] =  (string) $companyApptsBody->CompanyNumber;
                            $companyAppointments['has_inconsistencies'] = (string) $companyApptsBody->HasInconsistencies;
                            $companyAppointments['num_current_appt'] = (string) $companyApptsBody->NumCurrentAppt;
                            $companyAppointments['num_resigned_appt'] = (string) $companyApptsBody-> NumResignedAppt;
                            $companyAppointments['search_rows'] = (string) $companyApptsBody->SearchRows; 
                            $companyAppointments['continuation_key'] = (string) $companyApptsBody->ContinuationKey;

                            foreach ($companyApptsBody->CoAppt AS $appointment) {
                                $person = $appointment->Person;
                                $address = $person->PersonAddress;

                                $thisAppointmentDetails = array(
                                    'forename' => (string) $person->Forename,
                                    'surname' => (string) $person->Surname,
                                    'title' => (string) $person->Title,
                                    'honours' => (string) $person->Honours,
                                    'dob' => (string) $person->DOB,
                                    'nationality' => (string) $person->Nationality,
                                    'country_of_residence' => (string) $person->CountryOfResidence,
                                    'address' => array(
                                        'care_of' => (string) $address->CareOfName,
                                        'po_box' => (string) $address->POBox,
                                        'address_line' => (string) $address->AddressLine,
                                        'post_town' => (string) $address->PostTown,
					'county' => (string) $address->County,
					'country' => (string) $address->Country,
					'postcode' => (string) $address->Postcode
                                    ),
                                    'person_id' => (string) $person->PersonID,
                                    'number_of_appointments' => (string) $appointment->NumAppointments,
                                    'num_app_ind' => (string) $appointment->NumAppInd,
                                    'appoimtment_type' => (string) $appointment->AppointmentType,
                                    'appointment_status' => (string) $appointment->AppointmentStatus,
                                    'appointment_date_prefix' => (string) $appointment->ApptDatePrefix,
                                    'appointment_date' => (string) $appointment->AppointmentDate,
                                    'resignation_date' => (string) $appointment->ResignationDate,
                                    'occupation' => (string) $appointment->Occupation,
                                );
                                $companyAppointments['appointments'][$i] = $thisAppointmentDetails;
                                $i++;
                            }

                            return $companyAppointments;
			} else {
                            return false;
			}
            } else {
		return false;
            }
        }
 /* Details methods. */
	
	/**
	 * Gets information about the specified company. Processes a company
	 * DetailsRequest and returns the results.
	 *
	 * @param string $companyNumber The number of the company for which to return details.
	 * @param boolean $mortgageTotals Flag indicating if mortgage totals should be returned (if available).
	 * @return mixed An array packed with lots of exciting company data, or false on failure.
	 */
	public function companyDetails($companyNumber, $mortgageTotals = false) {
	
		if (preg_match('/[0-9]+/', $companyNumber)) { //[A-Z0-9]{8,8}

			$this->setMessageClass('CompanyDetails');


					global $wpdb;
					$companyDetailsBody = $wpdb->get_results( "SELECT * FROM companyHouse where id = '$companyNumber' LIMIT 1" );
					//var_dump($companyDetailsBody);exit;

						if ($companyDetailsBody !== null) {
							if (is_array($companyDetailsBody) && isset($companyDetailsBody[0]->id)) {
				$companyDetailsBody = $companyDetailsBody[0];
				
				
				
				foreach ($companyDetailsBody as $k => $v) {
				$companyDetails[$k] = $v;
				}
				
				$companyDetails['appointments'] = 1;
				
				return $companyDetails;
				
				$companyDetails = array('name' => (string) $companyDetailsBody->CompanyName,
				                        'number' => (string) $companyDetailsBody->id,
				                        //'category' => (string) $companyDetailsBody->CompanyCategory,
				                        'status' => (string) $companyDetailsBody->status,
				                        //'liquidation' => (string) $companyDetailsBody->InLiquidation,
				                        //'branchinfo' => (string) $companyDetailsBody->HasBranchInfo,
				                        //'appointments' => (string) $companyDetailsBody->HasAppointments);
				                        
				                        );
				                        
				                        
				                        
                     				return $companyDetails;
								
                             }
                         }


return false;


			
			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('CompanyDetailsRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/CompanyDetails.xsd');
				$package->writeElement('CompanyNumber', $companyNumber);
				if ($mortgageTotals === true) {
					$package->writeElement('GiveMortTotals', '1');
				}
			$package->endElement();
			
			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {

	 // Basic details...
				$companyDetailsBody = $this->getResponseBody()->CompanyDetails;
				$companyDetails = array('name' => (string) $companyDetailsBody->CompanyName,
				                        'number' => (string) $companyDetailsBody->CompanyNumber,
				                        'category' => (string) $companyDetailsBody->CompanyCategory,
				                        'status' => (string) $companyDetailsBody->CompanyStatus,
				                        'liquidation' => (string) $companyDetailsBody->InLiquidation,
				                        'branchinfo' => (string) $companyDetailsBody->HasBranchInfo,
				                        'appointments' => (string) $companyDetailsBody->HasAppointments);

	 // Dates...
				if (isset($companyDetailsBody->RegistrationDate)) {
					$companyDetails['registration_date'] = strtotime((string) $companyDetailsBody->RegistrationDate);
				}
				if (isset($companyDetailsBody->DissolutionDate)) {
					$companyDetails['dissolution_date'] = strtotime((string) $companyDetailsBody->DissolutionDate);
				}
				if (isset($companyDetailsBody->IncorporationDate)) {
					$companyDetails['incorporation_date'] = strtotime((string) $companyDetailsBody->IncorporationDate);
				}
				if (isset($companyDetailsBody->ClosureDate)) {
					$companyDetails['closure_date'] = strtotime((string) $companyDetailsBody->ClosureDate);
				}

	// Accounts and finance...
				if (isset($companyDetailsBody->Accounts)) {
					$companyDetails['accounts'] = array('overdue' => (string) $companyDetailsBody->Accounts->Overdue,
					                                    'document' => (string) $companyDetailsBody->Accounts->DocumentAvailable);
					if (isset($companyDetailsBody->Accounts->AccountRefDate)) {
						$companyDetails['accounts']['reference_date'] = (string) $companyDetailsBody->Accounts->AccountRefDate;
					}
					if (isset($companyDetailsBody->Accounts->NextDueDate)) {
						$companyDetails['accounts']['due_date'] = strtotime((string) $companyDetailsBody->Accounts->NextDueDate);
					}
					if (isset($companyDetailsBody->Accounts->LastMadeUpDate)) {
						$companyDetails['accounts']['last_madeup'] = strtotime((string) $companyDetailsBody->Accounts->LastMadeUpDate);
					}
					if (isset($companyDetailsBody->Accounts->AccountCategory)) {
						$companyDetails['accounts']['category'] = (string) $companyDetailsBody->Accounts->AccountCategory;
					}
				}
				if (isset($companyDetailsBody->Returns)) {
					$companyDetails['returns'] = array('overdue' => (string) $companyDetailsBody->Returns->Overdue,
					                                   'document' => (string) $companyDetailsBody->Returns->DocumentAvailable);
					if (isset($companyDetailsBody->Returns->NextDueDate)) {
						$companyDetails['returns']['due_date'] = strtotime((string) $companyDetailsBody->Returns->NextDueDate);
					}
					if (isset($companyDetailsBody->Returns->LastMadeUpDate)) {
						$companyDetails['returns']['last_madeup'] = strtotime((string) $companyDetailsBody->Returns->LastMadeUpDate);
					}
				}
				if (isset($companyDetailsBody->Mortgages)) {
					$companyDetails['mortgage'] = array('register' => (string) $companyDetailsBody->Mortgages->MortgageInd,
					                                    'charges' => (string) $companyDetailsBody->Mortgages->NumMortCharges,
					                                    'outstanding' => (string) $companyDetailsBody->Mortgages->NumMortOutstanding,
					                                    'part_satisfied' => (string) $companyDetailsBody->Mortgages->NumMortPartSatisfied,
					                                    'fully_satisfied' => (string) $companyDetailsBody->Mortgages->NumMortSatisfied);
				}

	 // Additional company details...
				if (isset($companyDetailsBody->PreviousNames)) {
					foreach ($companyDetailsBody->PreviousNames->CompanyName AS $previousName) {
						$companyDetails['previous_name'][] = (string) $previousName;
					}
				}
				foreach ($companyDetailsBody->RegAddress->AddressLine AS $addressLine) {
					$companyDetails['address'][] = (string) $addressLine;
				}
				foreach ($companyDetailsBody->SICCodes->SicText AS $sicItem) {
					$companyDetails['sic_code'][] = (string) $sicItem;
				}

				return $companyDetails;

			} else {
                                return false;
			}

		} else {
			return false;
		}
	
	}
	
	/**
	 * Gets information about the specified company officer. Processes a company
	 * DetailsRequest and returns the results.
	 *
	 * @param string $personId The ID of the person about whom to return details.
	 * @param string $reference A user reference which will be quoted in the billing breakdown.
	 * @return mixed An array of data about the officer, or false on failure.
	 */
	public function officerDetails($personId, $reference) {
	
		if (($personId != '') && (($reference != '') && (strlen($reference) < 25))) {
		
			$this->setMessageClass('OfficerDetails');





					global $wpdb;
					$officerDetailsBody = $wpdb->get_results( "SELECT company.recordid, company.name,  link.role, person.name as personName FROM link, person, company where person.id=link.person_id AND  link.company_id=company.id AND person.id = '$personId' LIMIT 100" );//GROUP BY company.id
					//var_dump($companyDetailsBody);exit;

						if ($officerDetailsBody !== null) {
							if (is_array($officerDetailsBody) ) {//echo count($officerDetailsBody); //print_r($officerDetailsBody);
							
							
				$officerDetails = array('title' => '',//(string) $officerDetailsBody->Person->Title,
				                        'honours' => '',//(string) $officerDetailsBody->Person->Honours,
				                        'surname' => (string) $officerDetailsBody[0]->personName,
				                        'dob' => '',//strtotime((string) $officerDetailsBody->Person->DOB),
				                        'nationality' => '',//(string) $officerDetailsBody->Person->Nationality
				                        );
                            
							$officerDetails['company'] = array();


                            foreach ($officerDetailsBody AS $appointment) {
								$officerDetails['company'][] = array('name' => (string) $appointment->name,
							                                                  'number' => (string) $appointment->recordid,
							                                                  'appoimtment_type' => (string) $appointment->role);

                            }
//var_dump($companyAppointments);exit;
//print_r($officerDetails);
                            return $officerDetails;                            
                            
                            
								
                             }
                         }


return false;





			
			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('OfficerDetailsRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/OfficerDetails.xsd');
				$package->writeElement('PersonID', $personId);
				$package->writeElement('UserReference', $reference);
			$package->endElement();
			
			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
				$officerDetailsBody = $this->getResponseBody()->OfficerDetails;
				
	 // Personal details...
				$officerDetails = array('title' => (string) $officerDetailsBody->Person->Title,
				                        'honours' => (string) $officerDetailsBody->Person->Honours,
				                        'surname' => (string) $officerDetailsBody->Person->Surname,
				                        'dob' => strtotime((string) $officerDetailsBody->Person->DOB),
				                        'nationality' => (string) $officerDetailsBody->Person->Nationality
                                                );

                                $officerDetails['continuation_key'] = (string) $officerDetailsBody->ContinuationKey;
                                $officerDetails['search_rows'] = (string) $officerDetailsBody->SearchRows;

				if (isset($officerDetailsBody->Person->Forename)) {
					if (count($officerDetailsBody->Person->Forename) > 1) {
						foreach ($officerDetailsBody->Person->Forename AS $forename) {
							$officerDetails['forename'][] = (string) $forename;
						}
					} else {
						$officerDetails['forename'] = (string) $officerDetailsBody->Person->Forename;
					}
				}
				if (isset($officerDetailsBody->Person->PersonAddress)) {
					$officerDetails['address'] = array();
					if (count($officerDetailsBody->Person->PersonAddress->AddressLine) > 1) {
						foreach ($officerDetailsBody->Person->PersonAddress->AddressLine AS $addressLine) {
							$officerDetails['address']['line'][] = (string) $addressLine;
						}
					} else {
						$officerDetails['address']['line'] = (string) $officerDetailsBody->Person->PersonAddress->AddressLine;
					}
					$officerDetails['address']['posttown'] = (string) $officerDetailsBody->Person->PersonAddress->PostTown;
					$officerDetails['address']['postcode'] = (string) $officerDetailsBody->Person->PersonAddress->Postcode;
				}

                                // Appointment counts. "Only available when officer has current appointments".
				if(isset($officerDetailsBody->ApptCount)) {
                                    $officerDetails['appt_count'] = array(
                                      'number_current_appointments' => (string) $officerDetailsBody->ApptCount->NumCurrentAppt,
                                      'number_dissolved_appointments' => (string) $officerDetailsBody->ApptCount->NumDissolvedAppt,
                                      'number_resigned_appointments' => (string) $officerDetailsBody->ApptCount->NumResignedAppt
                                    );
                                }
	 // Appointments...
				if (isset($officerDetailsBody->OfficerAppt)) {
					$officerDetails['company'] = array();
					foreach ($officerDetailsBody->OfficerAppt AS $appointment) {
						$arrayKey = (string) $appointment->CompanyNumber;
						if (!array_key_exists($arrayKey, $officerDetails['company'])) {
							$officerDetails['company'][$arrayKey] = array('name' => (string) $appointment->CompanyName,
							                                                  'number' => (string) $appointment->CompanyNumber,
							                                                  'status' => (string) $appointment->CompanyStatus);
						}
						$thisAppointment = array('type' => (string) $appointment->AppointmentType,
						                         'status' => (string) $appointment->AppointmentStatus,
						                         'date' =>  strtotime((string) $appointment->AppointmentDate));
                                                if (isset($appointment->ApptDatePrefix)) {
                                                    $thisAppointment['appt_date_prefix'] = (string) $appointment->ApptDatePrefix;
                                                }

						if (isset($appointment->ResignationDate)) {
							$thisAppointment['resignation'] = strtotime((string) $appointment->ResignationDate);
						}
						if (isset($appointment->Occupation)) {
							$thisAppointment['occupation'] = (string) $appointment->Occupation;
						}
						$officerDetails['company'][$arrayKey]['appointment'][] = $thisAppointment;
					}
				}
				
	 // Disqualifcations...
				if (isset($officerDetailsBody->OfficerDisq)) {
					foreach ($officerDetailsBody->OfficerDisq AS $disqualifcation) {
						$thisDisqualifcation = array('reason' => (string) $disqualifcation->DisqReason,
						                             'start' => strtotime((string) $disqualifcation->StartDate),
						                             'end' => strtotime((string) $disqualifcation->EndDate));
						if (isset($disqualifcation->Exemption)) {
							foreach ($disqualifcation->Exemption AS $exemption) {
								$thisDisqualifcation['exemption'][] = array('name' => $exemption->CompanyName,
								                                            'number' => $exemption->CompanyNumber,
								                                            'start' => strtotime((string) $exemption->StartDate),
								                                            'end' => strtotime((string) $exemption->EndDate));
							}
						}
						$officerDetails['disqualifcation'][] = $thisDisqualifcation;
					}
				}
				
				return $officerDetails;
				
			} else {
				return false;
			}
			
		} else {
			return false;
		}
	
	}
	
	/**
	 * Processes a company FilingHistoryRequest and returns the results.
	 *
	 * @param string $companyNumber The number of the company for which to return filing history.
	 * @param boolean $capitalDocs Flag indicating if capital documents should be returned (if available).
	 * @return mixed An array containing the filing history inclduing document keys, or false on failure.
	 */
	public function filingHistory($companyNumber, $capitalDocs = false) {

		if (preg_match('/[A-Z0-9]{8,8}/', $companyNumber)) {

			$this->setMessageClass('FilingHistory');

			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('FilingHistoryRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/FilingHistory.xsd');
				$package->writeElement('CompanyNumber', $companyNumber);
				if ($capitalDocs === true) {
					$package->writeElement('CapitalDocInd', '1');
				}
			$package->endElement();
			
			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
				$filingHistoryBody = $this->getResponseBody()->FilingHistory;
				if (isset($filingHistoryBody->FHistItem)) {
					$filingHistory = array();
					foreach ($filingHistoryBody->FHistItem AS $historyItem) {
						$thisHistoryItem = array('date' => strtotime((string) $historyItem->DocumentDate),
						                         'type' => (string) $historyItem->FormType);
						foreach ($historyItem->DocumentDesc AS $documentDescription) {
							$thisHistoryItem['description'][] = (string) $documentDescription;
						}
						if (isset($historyItem->DocBeingScanned)) {
							$thisHistoryItem['pending'] = (string) $historyItem->DocBeingScanned;
						}
						if (isset($historyItem->ImageKey)) {
							$thisHistoryItem['key'] = (string) $historyItem->ImageKey;
						}
						$filingHistory[] = $thisHistoryItem;
					}
					return $filingHistory;
				} else {
					return false;
				}

			} else {
				return false;
			}

		} else {
			return false;
		}

	}
	
	/**
	 * Gets information about a specific document returned by the filing history
	 * call. Processes a DocumentInfoRequest call and returns the results.
	 *
	 * @param string $companyNumber The number of the company for which to return the document information.
	 * @param boolean $companyName The name of the company for which to return the document information.
	 * @param boolean $imageKey The key returned from a filing history request of the document in question.
	 * @return mixed An array containing information on the document in question, or false on failure.
	 */
	public function documentInfo($companyNumber, $companyName, $imageKey) {

		if (preg_match('/[A-Z0-9]{8,8}/', $companyNumber) && (($companyName != '') && (strlen($companyName) < 161))) {
		
			$this->setMessageClass('DocumentInfo');
			
			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('DocumentInfoRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/CompanyDocument.xsd');
				$package->writeElement('CompanyNumber', $companyNumber);
				$package->writeElement('CompanyName', $companyName);
				$package->writeElement('ImageKey', $imageKey);
			$package->endElement();
			
			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
			
				$documentInfoBody = $this->getResponseBody()->DocumentInfo;
				$documentInfo = array('type' => (string) $documentInfoBody->FormType,
				                      'pages' => (string) $documentInfoBody->NumPages,
				                      'status' => (string) $documentInfoBody->Media,
				                      'key' => (string) $documentInfoBody->DocRequestKey);
				if (isset($documentInfoBody->MadeUpDate)) {
					$documentInfo['date'] = strtotime((string) $documentInfoBody->MadeUpDate);
				}
				return $documentInfo;
				
			} else {
				return $this->getErrors();
				return false;
			}
			
		} else {
			return "Wrong source data";
			return false;
		}
	
	}
	
	/**
	 * Requests a document from the document ordering system. As documents may
	 * take time to prepare this function returns the FTP location used to fetch
	 * the document image, as well as a poll interval (in seconds) which should
	 * be used to re-poll the server until the document is ready.
	 *
	 * @param string $documentKey The key provided by a document info request identifying the document in question.
	 * @param string $reference A user reference which will be quoted in the billing breakdown.
	 * @return mixed An array of the document endpoint and retry interval, or false on failure.
	 */
	public function documentRequest($documentKey, $reference) {
	
		if (($documentKey != '') && (($reference != '') && (strlen($reference) < 25))) {
		
			$this->setMessageClass('Document');
			
			$package = new XMLWriter();
			$package->openMemory();
			$package->setIndent(true);
			$package->startElement('DocumentRequest');
				$package->writeAttribute('xsi:noNamespaceSchemaLocation', 'http://xmlgw.companieshouse.gov.uk/v1-0/schema/CompanyDocument.xsd');
				$package->writeElement('DocRequestKey', $documentKey);
				$package->writeElement('UserReference', $reference);
			$package->endElement();
			
			$this->setMessageBody($package);
			$this->addChannelRoute('http://blogs.fubra.com/php-govtalk/extensions/companieshouse/', 'php-govtalk Companies House Extension', $this->_extensionVersion);
			if ($this->sendMessage() && ($this->responseHasErrors() === false)) {
				return $this->getResponseEndpoint();
			}
			
		} else {
			return false;
		}
	
	}

 /* Protected methods. */
 
	/**
	 * Generates the token required to authenticate with the XML Gateway.  This
	 * function assumes the Gateway username and password have already been
	 * defined.  It over-rides the GovTalk class'
	 * _generateAlternativeAuthentication() method.
	 *
	 * @param string $transactionId Transaction ID to use generating the token.
	 * @return mixed The authentication array, or false on failure.
	 */
	protected function generateAlternativeAuthentication($transactionId) {

		if (is_numeric($transactionId)) {
			$authenticationArray = array('method' => 'CHMD5',
			                             'token' => md5($this->_govTalkSenderId.$this->_govTalkPassword.$transactionId));
			return $authenticationArray;
		} else {
			return false;
		}

	}
 
	private function _parseCompanyDocumentsResult($companySearchBody) {
		if (is_object($companySearchBody) && is_a($companySearchBody, 'SimpleXMLElement')) {
			$exactCompany = $possibleCompanies = array();
			foreach ($companySearchBody->CoSearchItem AS $possibleCompany) {
				$thisCompanyDetails = array('name' => (string) $possibleCompany->CompanyName,
				                            'number' => (string) $possibleCompany->CompanyNumber);
				$possibleCompanies[] = $thisCompanyDetails;
				if (isset($possibleCompany->SearchMatch) && ((string) $possibleCompany->SearchMatch == 'EXACT')) {
					$exactCompany = $thisCompanyDetails;
				}
			}
			return array('exact' => $exactCompany,
			             'match' => $possibleCompanies);
		} else {
			return false;
		}
	}
 /* Private methods. */
 
	/**
	 * Parses the partial output of a CompanySearch result into an array.
	 *
	 * @param string $companySearchBody The body of the CompanySearch response.
	 * @return mixed An array 'exact' => any match marked as exact by CH, 'match' => all matches returned by CH, or false on failure.
	 */
	private function _parseCompanySearchResult($companySearchBody) {
	
		if (is_object($companySearchBody) && is_a($companySearchBody, 'SimpleXMLElement')) {
			$exactCompany = $possibleCompanies = array();
			foreach ($companySearchBody->CoSearchItem AS $possibleCompany) {
				$thisCompanyDetails = array('name' => (string) $possibleCompany->CompanyName,
				                            'number' => (string) $possibleCompany->CompanyNumber);
				$possibleCompanies[] = $thisCompanyDetails;
				if (isset($possibleCompany->SearchMatch) && ((string) $possibleCompany->SearchMatch == 'EXACT')) {
					$exactCompany = $thisCompanyDetails;
				}
			}
			return array('exact' => $exactCompany,
			             'match' => $possibleCompanies);
		} else {
			return false;
		}
	
	}
	
}
