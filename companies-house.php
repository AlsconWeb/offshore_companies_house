<?php
/**
 * Plugin Name: Companies House.
 * Plugin URI: http://mister.ukrweb.net/word/wordpress
 * Description: Companies House search.
 * Version: 0.0.10
 * Author: Sergey Balitsky & Eugene Babin
 * Author URI: http://mistersoft.org
 * Text Domain: companies-house
 * Domain Path: /locale/
 * License: GPL2
 */

if ( ! class_exists( 'Companies_House' ) ) {
	class Companies_House {
		/**
		 * Construct the plugin object
		 */
		public function __construct() {
			$this->define_constants();
			// Auto-load classes on demand
			if ( function_exists( "__autoload" ) ) {
				spl_autoload_register( "__autoload" );
			}

			add_shortcode( 'ch_address_search_form', [ __CLASS__, 'ch_address_search_func' ] );

			add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );


			spl_autoload_register( [ $this, 'autoload' ] );

			add_action( 'init', [ 'CP_Plugin', 'init' ] );
			load_plugin_textdomain( 'companies-house', false, plugin_basename( dirname( __FILE__ ) ) . '/locale/' );
		}

		/**
		 * Add Script and style.
		 *
		 * @return void
		 */
		public function add_scripts(): void {
			wp_register_style( 'ch_address_search', WP_PLUGIN_URL . '/ch-search-plugin_2/public/css/main.css', '', '0.0.10', '' );
			wp_enqueue_style( 'ch_address_search' );

		}

		/**
		 * Activate the plugin
		 */
		public static function activate() {
			CP_Install::install();
		} // END public static function activate

		/**
		 * Deactivate the plugin
		 */
		public static function deactivate() {
			CP_Install::deinstall();
		} // END public static function deactivate

		/**
		 * hook into WP's admin_init action hook
		 */
		public function admin_init() {
			// Set up the settings for this plugin
			$this->init_settings();
			// Possibly do additional admin_init tasks
		} // END public static function activate

		/**
		 * Initialize some custom settings
		 */
		public function init_settings() {
			// register the settings for this plugin
			// XML API Login information
			register_setting( 'Companies_House-group', 'ch_xml_username' );
			register_setting( 'Companies_House-group', 'ch_xml_password' );
			// XML Request info
			register_setting( 'Companies_House-group', 'ch_xml_email' );
			// Company search
			register_setting( 'Companies_House-group', 'ch_xml_morttotals' );
			// Company appointments
			register_setting( 'Companies_House-group', 'ch_xml_appresigned' );
			// officer search
			register_setting( 'Companies_House-group', 'ch_xml_offtype' );
			register_setting( 'Companies_House-group', 'ch_xml_offresigned' );
			// WooCommerce Products
			register_setting( 'Companies_House-group', 'ch_xml_doc_product' );
			register_setting( 'Companies_House-group', 'ch_xml_off_product' );
		} // END public function init_custom_settings()

		/**
		 * add a menu
		 */
		public function add_menu() {
			add_options_page( 'WP Companies Panama Settings', 'WP Companies Panama', 'manage_options', 'Companies_House', [
				&$this,
				'plugin_settings_page',
			] );
		} // END public function add_menu()

		/**
		 * Menu Callback
		 */
		public function plugin_settings_page() {
			if ( ! current_user_can( 'manage_options' ) ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
			}

			// Render the settings template
			include( sprintf( "%s/templates/settings.php", dirname( __FILE__ ) ) );
		} // END public function plugin_settings_page()

		public function define_constants() {
			define( "CP_PLUGIN_PATH", untrailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		public function autoload( $class ) {
			$path  = null;
			$class = strtolower( $class );
			$file  = 'class-' . str_replace( '_', '-', $class ) . '.php';
			$path  = $this->plugin_path() . '/classes/';

			if ( $path && is_readable( $path . $file ) ) {
				include_once( $path . $file );

				return;
			}
			switch ( $class ) {
				case 'companieshousepanama':
					include_once( $this->plugin_path() . '/lib/php-govtalk/extensions/CompaniesHousePanama/CompaniesHousePanama.php' );

					return;
				case 'govtalk':
					include_once( $this->plugin_path() . '/lib/php-govtalk/GovTalk.php' );

					return;
			};

			return false;
		}

		/**
		 * Get the plugin path.
		 *
		 * @return string
		 */
		public function plugin_path() {
			return CP_PLUGIN_PATH;
		}


		static function ch_address_search_func() {
			ob_start();
			?>
			<div id="search-app" class="col-xs-12">

				<form class="form-horizontal" method="get" action="/blog/company_house">
					<input type="search" name="search_company" class="form-control" placeholder="Введите адрес компании"
						   value="<?= isset( $_GET['search_company'] ) ? $_GET['search_company'] : '' ?>">
					<input type="submit" value="Поиск">
				</form>
			</div>
			<?php
			$out = ob_get_contents();
			ob_end_clean();

			return $out;
		}

	}

	// Installation and uninstallation hooks
	register_activation_hook( __FILE__, [ 'Companies_House', 'activate' ] );
	register_deactivation_hook( __FILE__, [ 'Companies_House', 'deactivate' ] );

	// instantiate the plugin class
	$Companies_House = new Companies_House();
} // EOF if (!class_exists('Companies_House')) {
// Add a link to the settings page onto the plugin page
if ( isset( $Companies_House ) ) {
	// Add the settings link to the plugins page
	function cp_plugin_settings_link( $links ) {
		$settings_link = '<a href="options-general.php?page=Companies_House">Settings</a>';
		array_unshift( $links, $settings_link );

		return $links;
	}

	$plugin = plugin_basename( __FILE__ );
	add_filter( "plugin_action_links_$plugin", 'cp_plugin_settings_link' );
}
